use anchor_lang::prelude::*;
pub mod instructions;
pub mod errors;
pub mod constants;
pub mod helper_functions;
use instructions::*;

//use pyth_anchor;
//declare_id!("Fg6PaFpoGXkYsidMpWTK6W2BeZ7FEfcYkg476zPFsLnS");
declare_id!("HumhEvLnBqFsaGeSv7QBhHUbBwkap3JQppWLTEuAxkH1");
// declare_id!("DsdR9Ly8RUvzP6SRSkHWqVaojSBUdZVDobzRDV471HZN");

#[program]
pub mod escrow {
    use super::*;

    pub fn initialize_option(
        ctx: Context<InitializeOption>,
        oracle_asset_key: Pubkey, // Needs validating i.e. if this is a real pyth oracle asset
        expiry: u64,
        strike: u64,
        strike_exponent: i32,
        oa_bump:u8,
        vault_bump: u8,
        long_mint_bump: u8,
        short_mint_bump: u8,
    ) -> ProgramResult {
        instructions::initialize_option::handler(
            ctx,
            oracle_asset_key,
            expiry,
            strike,
            strike_exponent,
            oa_bump,
            vault_bump,
            long_mint_bump,
            short_mint_bump
        )
    }

    pub fn open_new_trade(
        ctx: Context<OpenNewTrade>,
        oracle_asset_key: Pubkey, // Needs validating against oa
        option_account : Pubkey, // Option object for underlying
        oa_bump: u8,
        strike: u64,
        strike_exponent: i32,
        expiry: u64,
        quantity: u64,
        price: u64,
        is_long: bool, // true for long, false for short. Money won if price > strike
        is_close: bool, // If sell order check and don't take money. Also check if no other sell order on book. 
        trader_mint_bump: u8,
    ) -> ProgramResult {
        instructions::open_new_trade::handler(
            ctx,
            oracle_asset_key,
            option_account,
            oa_bump,
            strike,
            strike_exponent,
            expiry,
            quantity,
            price,
            is_long,
            is_close,
            trader_mint_bump
        )
    }

    pub fn cancel_trade(
        ctx: Context<CancelTrade>,
        oracle_asset_key: Pubkey,
        option_account : Pubkey, // Option object for underlying
        oa_bump: u8,
        strike: u64,
        strike_exponent: i32,
        expiry: u64,
        quantity: u64,
        price: u64,
        is_long: bool, // true for long, false for short. Money won if price > strike
        is_close: bool, // If sell order check and don't take money. Also check if no other sell order on book. 
        vault_bump: u8
    ) -> ProgramResult {
        instructions::cancel_trade::handler(
            ctx,
            oracle_asset_key,
            option_account,
            oa_bump,
            strike,
            strike_exponent,
            expiry,
            quantity,
            price,
            is_long,
            is_close,
            vault_bump
        )
    }

    pub fn match_order(
        ctx: Context<MatchOrder>,
        oracle_asset_key: Pubkey,
        long_mint_bump: u8,
        short_mint_bump: u8,
        expiry: u64,
        strike: u64,
        strike_exponent: i32, 
        amount: u64,
        long_trader_mint_bump: u8,
        short_trader_mint_bump: u8,
        vault_bump: u8,
    ) -> ProgramResult {
        instructions::match_order::handler(
            ctx,
            oracle_asset_key,
            long_mint_bump,
            short_mint_bump,
            expiry,
            strike,
            strike_exponent,
            amount,
            long_trader_mint_bump,
            short_trader_mint_bump,
            vault_bump
        )
    }

    pub fn settle_option(
        ctx: Context<SettleOption>,
        oracle_asset_key: Pubkey, // Needs validating against oa
        oa_bump: u8,
        strike: u64,
        strike_exponent: i32,
        expiry: u64,
        long_mint_bump: u8,
        short_mint_bump: u8,

    ) -> ProgramResult {
        instructions::settle_option::handler(
            ctx,
            oracle_asset_key,
            oa_bump,
            strike,
            strike_exponent,
            expiry,
            long_mint_bump,
            short_mint_bump,
        )
    }

    pub fn collect_option(
        ctx: Context<CollectOption>,
        oracle_asset_key: Pubkey,
        expiry: u64,
        strike: u64,
        strike_exponent: i32, 
        amount: u64,
        long_mint_bump: u8,
        short_mint_bump: u8,
        oa_bump: u8,
        vault_bump: u8,
        trader_mint_bump: u8,

    ) -> ProgramResult {
        instructions::collect_option::handler(
            ctx,
            oracle_asset_key,
            expiry,
            strike,
            strike_exponent, 
            amount,
            long_mint_bump,
            short_mint_bump,
            oa_bump,
            vault_bump,
            trader_mint_bump,
            
        )
    }
}

use anchor_lang::prelude::*;

#[error]
pub enum ErrorCode {
    #[msg("Option has Settled Already")]
    OptionSettled,

    #[msg("Option has NOT Settled Already")]
    OptionNotSettled,

    #[msg("Option has been messed with")]
    OptionSettledOrMessed,

    #[msg("MINT MISMATCH. Option has been messed with")]
    OptionMintMismatch,

    #[msg("Option Expiry is in the Past")]
    ExpiryInThePast,

    #[msg("Option Expiry is in the Future")]
    ExpiryInTheFuture,

    #[msg("Invalid Asset")]
    InvalidAsset,

    #[msg("Invalid Oracle")]
    InvalidOracle,

    #[msg("Price outside 1-99")]
    PriceError,

    #[msg("Closing Trade quantity is more than existing position")]
    CloseTradeError,

    #[msg("Closing Trade quantity is more than existing position in Match Order")]
    CloseMatchingError,

    #[msg("Cancelling order that does not match the order on Order Book")]
    CancelOrderDetailsError,

    #[msg("Long quantity is less than 1")]
    LongQuantityError,

    #[msg("Short quantity is less than 1")]
    ShortQuantityError,

    #[msg("Order is not at the top of Long Orderbook. Try again")]
    LongOrderBookError,

    #[msg("Order is not at the top of Short Orderbook. Try again")]
    ShortOrderBookError,

    #[msg("Long Orderbook is full. enter a better price")]
    LongOrderBookFullError,

    #[msg("Short Orderbook is full. enter a better price")]
    ShortOrderBookFullError,

    #[msg("Not enough lamports in account")]
    InsufficientFundError,

}

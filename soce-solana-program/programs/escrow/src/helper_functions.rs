use anchor_lang::prelude::*;
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::program::invoke_signed;
use anchor_lang::solana_program::system_instruction;
use anchor_spl::associated_token::AssociatedToken;
use anchor_spl::token::{
    self,
    Burn,
    Mint,
    MintTo,
    SetAuthority,
    Token,
    TokenAccount,
    Transfer
};
use anchor_lang::Id;

pub fn transfer_from_our_pda(
    src: &mut AccountInfo, //  better own this account though
    dst: &mut AccountInfo,
    amount: u64,
) -> ProgramResult {
    **src.try_borrow_mut_lamports()? = src
        .lamports()
        .checked_sub(amount)
        .ok_or(ProgramError::InvalidArgument)?;
    **dst.try_borrow_mut_lamports()? = dst
        .lamports()
        .checked_add(amount)
        .ok_or(ProgramError::InvalidArgument)?;
        msg!("$$$$$$$$$$END: SEND MONEY#########");
    Ok(())
}


//     amount_to_refund = quantity * price * (10^constants::PRICE_EXPONENT as u64);
//     let ix = system_instruction::transfer(
//         ctx.accounts.vault_account.to_account_info().key,
//         ctx.accounts.trader.to_account_info().key,
//         amount_to_refund,
//     );

//     invoke(
//         &ix,
//         &[
//             ctx.accounts.vault_account.to_account_info(),
//             ctx.accounts.trader.to_account_info(),
//             ctx.accounts.system_program.to_account_info(),
//         ],
//     )?;

// TODOs
//  1) CHECK PDAs of all accounts
//  2) change long to bull option and short to bear option
//  3) use require macro - 
//     require!(long_order.trader.key() != ctx.accounts.long_trader.key(), ErrorCode::LongOrderBookError); 
//  4) If sum of long and short price is more than hundred. Refund some.
//     For now we keep it 
// 5) Fees to be added later
//     let fees = quantity * price * (10^(constants::PRICE_EXPONENT - 2) as u64);
//     Start with nominal fees to cover money txn cost
// 6) Uncomment  in prd:
//     return Err(ErrorCode::ExpiryInTheFuture.into());

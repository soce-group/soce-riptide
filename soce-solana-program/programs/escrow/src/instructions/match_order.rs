use anchor_lang::prelude::*;
use anchor_spl::token::{
  self,
  Burn,
  Mint,
  MintTo,
  SetAuthority,
  Token,
  TokenAccount,
  Transfer
};
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::system_instruction;
use crate::OptionAccount;
use crate::errors::ErrorCode;
use crate::OrderInfo;
use crate::constants;
use crate::helper_functions;

pub fn handler(
  ctx: Context<MatchOrder>,
  oracle_asset_key: Pubkey,
  long_mint_bump: u8,
  short_mint_bump: u8,
  expiry: u64,
  strike: u64,
  strike_exponent: i32, 
  amount: u64,
  long_trader_mint_bump: u8,
  short_trader_mint_bump: u8,
  vault_bump:u8,
) -> ProgramResult {
  msg!("---------BEGIN: match_trade---------");

  if ctx.accounts.option_account.is_settled{
    return Err(ErrorCode::OptionSettled.into());
  }

  let long_trader_mint_seeds = [
    ctx.accounts.long_trader.key.as_ref(),
    &[long_trader_mint_bump]
  ];

  let short_trader_mint_seeds = [
    ctx.accounts.short_trader.key.as_ref(),
    &[short_trader_mint_bump]
  ];

  let long_mint_seeds = [
    oracle_asset_key.as_ref(),
    &expiry.to_le_bytes(),
    &strike.to_le_bytes(),
    &strike_exponent.to_le_bytes(),
    b"long",
    &[long_mint_bump]
  ];

  let short_mint_seeds = [
    oracle_asset_key.as_ref(),
    &expiry.to_le_bytes(),
    &strike.to_le_bytes(),
    &strike_exponent.to_le_bytes(),
    b"short",
    &[short_mint_bump]
  ];

 
  // let short_mint_to_long_trader = token::MintTo {
  //   mint: ctx.accounts.short_mint_account.to_account_info(),
  //   to: ctx.accounts.long_trader_short_mint_account.to_account_info(),
  //   authority: ctx.accounts.short_mint_account.to_account_info(),
  // };

  // let long_mint_to_short_trader = token::MintTo {
  //   mint: ctx.accounts.long_mint_account.to_account_info(),
  //   to: ctx.accounts.short_trader_long_mint_account.to_account_info(),
  //   authority: ctx.accounts.long_mint_account.to_account_info(),
  // };
    // let short_burn_short_trader = token::Burn {
  //   mint: ctx.accounts.short_mint_account.to_account_info(),
  //   to: ctx.accounts.short_trader_short_mint_account.to_account_info(),
  //   authority: ctx.accounts.long_trader_mint_pda.to_account_info(),
  // };

  let long_mint_to_long_trader = token::MintTo {
    mint: ctx.accounts.long_mint_account.to_account_info(),
    to: ctx.accounts.long_trader_long_mint_account.to_account_info(),
    authority: ctx.accounts.long_mint_account.to_account_info(),
  };

  let short_mint_to_short_trader = token::MintTo {
    mint: ctx.accounts.short_mint_account.to_account_info(),
    to: ctx.accounts.short_trader_short_mint_account.to_account_info(),
    authority: ctx.accounts.short_mint_account.to_account_info(),
  };

  
  let short_burn_long_trader = token::Burn {
    mint: ctx.accounts.short_mint_account.to_account_info(),
    to: ctx.accounts.long_trader_short_mint_account.to_account_info(),
    authority: ctx.accounts.long_trader_mint_pda.to_account_info(),
  };

  let long_burn_short_trader = token::Burn {
    mint: ctx.accounts.long_mint_account.to_account_info(),
    to: ctx.accounts.short_trader_long_mint_account.to_account_info(),
    authority: ctx.accounts.short_trader_mint_pda.to_account_info(),
  };

  ctx.accounts.option_account.long_orderbook.sort_by(|a, b| b.price.cmp(&a.price));
  ctx.accounts.option_account.short_orderbook.sort_by(|a, b| b.price.cmp(&a.price));
  let long_order: OrderInfo = ctx.accounts.option_account.long_orderbook[0];
  let short_order: OrderInfo = ctx.accounts.option_account.short_orderbook[0];

  msg!("++++++++++++++ long order- {:?}", long_order);
  msg!("++++++++++++++ short order- {:?}", short_order); 

  //TODO use require macro
  //require!(long_order.trader.key() != ctx.accounts.long_trader.key(), ErrorCode::LongOrderBookError); 

  if long_order.trader.key() != ctx.accounts.long_trader.key(){
    msg!("---------Here: match_trade - LongOrderBookError---------");
     return Err(ErrorCode::LongOrderBookError.into() );
  }

  if short_order.trader.key() != ctx.accounts.short_trader.key() {
    msg!("---------Here: match_trade - ShortOrderBookError---------");
     return Err(ErrorCode::ShortOrderBookError.into());
  }

  //TODO if sum of long and short price is more than hundred. Refund some.
  // For now we keep it 
  if  short_order.price + long_order.price < 100  {
    msg!("---------Here: match_trade - PriceError---------");
     return Err(ErrorCode::PriceError.into());
  }
  
  if short_order.quantity < 1 {
    msg!("---------Here: match_trade - ShortQuantityError---------");
    return Err(ErrorCode::ShortQuantityError.into());
  }

  if long_order.quantity < 1 {
    msg!("---------Here: match_trade - LongQuantityError---------");
    return Err(ErrorCode::LongQuantityError.into());
  }

  let mut pop_long_order: bool = false;
  let mut pop_short_order: bool = false;
  let mut matched_qty: u64 = 0;
  // Using min of the two quantities
  if long_order.quantity == short_order.quantity {
    matched_qty = short_order.quantity;
    pop_long_order = true;
    pop_short_order = true;
  }
  else if long_order.quantity > short_order.quantity {
    matched_qty = short_order.quantity;
    pop_short_order = true;
  } 
  else {
    matched_qty = long_order.quantity;
    pop_long_order = true;
  };
  msg!("long_order.quantity - {:?}", long_order.quantity);
  msg!("short_order.quantity - {:?}", short_order.quantity);
  msg!("matched_qty - {:?}", matched_qty);
  
  msg!("before: long_trader_short_mint_account.amount - {:?}", ctx.accounts.long_trader_short_mint_account.amount);
  msg!("before: long_trader_long_mint_account.amount - {:?}", ctx.accounts.long_trader_long_mint_account.amount);
  msg!("before: short_trader_short_mint_account.amount - {:?}", ctx.accounts.short_trader_short_mint_account.amount);
  msg!("before: short_trader_long_mint_account.amount - {:?}", ctx.accounts.short_trader_long_mint_account.amount);


  //LONG TRADER CODE
  //2 cases here:
  //  Long Trader is actually a short position owner closing the position
  //  Or a trader putting on new posititon.
  //  We dont support crossing from long to short as in I have 10 short and close with 20 long so that I can get 10 long
  //  To do that enter 2 orders. One closing 10 short and one opening 10 long. 
  
  // Case 1: Closing Trade
  //If long trader has short tokens, burn them and pay money back  
  if long_order.is_close {
    if ctx.accounts.long_trader_short_mint_account.amount < matched_qty || ctx.accounts.long_trader_short_mint_account.amount < 1 {
      msg!("---------: match_trade - Matching qty is more than tokens in account for closing long trader---------");
      return Err(ErrorCode::CloseMatchingError.into());
    };
    let tokens_to_burn: u64 =  matched_qty;

    //BURN TOKENS
    let short_burn_long_trader_result = token::burn(
      CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        short_burn_long_trader,
        &[&long_trader_mint_seeds],
      ),
      tokens_to_burn,
    );
    msg!("short_burn_long_trader_result - {:?}", short_burn_long_trader_result);

    //ISSUE CREDIT.
    // We need to do 100-px cause we did 100-px earlier when we put order in OB. SO this gets the trader entered price.
    // We did that cause we had to put it on the long order book . 
    //The burn of short token corresponds to issue of short token to short_order/short_trader. 
    // SO total option stay same. And money the other short gave goes to this guy
    let amount_to_refund: u64 = tokens_to_burn * (100 - long_order.price) * (10^constants::PRICE_EXPONENT as u64);

    let transfer_result = helper_functions::transfer_from_our_pda(
      &mut ctx.accounts.vault_account.to_account_info(),
      &mut ctx.accounts.long_trader.to_account_info(),
      amount_to_refund
    );
  
    // if remaining_qty > 0 {
    //   let long_mint_to_long_trader_result = token::mint_to(
    //     CpiContext::new_with_signer(
    //       ctx.accounts.token_program.to_account_info(),
    //       long_mint_to_long_trader,
    //       &[&long_mint_seeds],
    //     ),
    //     remaining_qty,
    //   );
    //   msg!("long_mint_to_long_trader_result - {:?}", long_mint_to_long_trader_result);
    // }
 }
  // Long Trade is not closing trade
  else { 
    let long_mint_to_long_trader_result = token::mint_to(
      CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        long_mint_to_long_trader,
        &[&long_mint_seeds],
      ),
      matched_qty,
    );
    msg!("long_mint_to_long_trader_result - {:?}", long_mint_to_long_trader_result);
  }


  //SHORT TRADER CODE

  //2 cases here:
  //  Short Trader is actually a long position owner closing the position
  //  Or a trader putting on new posititon.
  //  We dont support crossing from short to long as in I have 10 long and close with 20 short so that I can get 10 short
  //  To do that enter 2 orders. One closing 10 long and one opening 10 short. 
  
  // Case 1: Closing Trade
  //If short trader has long tokens, burn them and pay money back   

  if short_order.is_close {
    if ctx.accounts.short_trader_long_mint_account.amount < matched_qty || ctx.accounts.short_trader_long_mint_account.amount < 1 {
      msg!("---------: match_trade - Matching qty is more than tokens in account for closing short trader---------");
      return Err(ErrorCode::CloseMatchingError.into());
    };
    let tokens_to_burn: u64 =  matched_qty;


    //BURN TOKENS
    let long_burn_short_trader_result = token::burn(
      CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        long_burn_short_trader,
        &[&short_trader_mint_seeds],
      ),
      tokens_to_burn,
    );
    msg!("long_burn_short_trader_result - {:?}", long_burn_short_trader_result);

    //ISSUE CREDIT.
    // We need to do 100-px cause we did 100-px earlier when we put order in OB. SO this gets the trader entered price.
    // We did that cause we had to put it on the short order book . 
    //The burn of long token corresponds to issue of long token to long_order/long_trader. 
    // SO total option stay same. And money the other long gave goes to this guy
    let amount_to_refund: u64 = tokens_to_burn * (100 - short_order.price) * (10^constants::PRICE_EXPONENT as u64);
    let transfer_result = helper_functions::transfer_from_our_pda(
      &mut ctx.accounts.vault_account.to_account_info(),
      &mut ctx.accounts.short_trader.to_account_info(),
      amount_to_refund
    );
  } 

  // Short Trade is not closing trade
  else {
    let short_mint_to_short_trader_result = token::mint_to(
      CpiContext::new_with_signer(
        ctx.accounts.token_program.to_account_info(),
        short_mint_to_short_trader,
        &[&short_mint_seeds],
      ),
      matched_qty,
    );
    msg!("short_mint_to_short_trader_result - {:?}", short_mint_to_short_trader_result);
  }

  //Removing order from orderbook
  if pop_long_order{
    ctx.accounts.option_account.long_orderbook.remove(0);
  }
  if pop_short_order{
    ctx.accounts.option_account.short_orderbook.remove(0);
  }

  msg!("---------END: match_trade-----------");
  Ok(())
}

#[derive(Accounts)]
#[instruction(
  oracle_asset_key: Pubkey,
  long_mint_bump: u8,
  short_mint_bump: u8,
  expiry: u64,
  strike: u64,
  strike_exponent: i32, 
  amount: u64,
  long_trader_mint_bump: u8,
  short_trader_mint_bump: u8,
  vault_bump:u8,
)]
pub struct MatchOrder<'info> {

      /// CHECK: unsafe
    #[account(
        mut,
        constraint = option_account.oracle_asset_key == oracle_asset_key, 
    )]
    pub option_account: Box<Account<'info, OptionAccount>>,
  
    /// CHECK: unsafe
    #[account(
    )]
    pub long_trader: AccountInfo<'info>,

    /// CHECK: unsafe
    #[account(
        seeds = [ long_trader.key.as_ref() ],
        bump
    )]
    pub long_trader_mint_pda: AccountInfo<'info>,

    /// CHECK: unsafe
    #[account(
      associated_token::mint = long_mint_account,
      associated_token::authority = long_trader_mint_pda.key,
      mut
    )]
    pub long_trader_long_mint_account: Box<Account<'info, TokenAccount>>,

    /// CHECK: unsafe
    #[account(
      associated_token::mint = short_mint_account,
      associated_token::authority = long_trader_mint_pda.key,
      mut
    )]
    pub long_trader_short_mint_account: Box<Account<'info, TokenAccount>>,

    /// CHECK: unsafe
    #[account(
    )]
    pub short_trader: AccountInfo<'info>,

    /// CHECK: unsafe
    #[account(
        seeds = [ short_trader.key.as_ref() ],
        bump
    )]
    pub short_trader_mint_pda: AccountInfo<'info>,

    /// CHECK: unsafe
    #[account(
      associated_token::mint = long_mint_account,
      associated_token::authority = short_trader_mint_pda.key,
      mut
    )]
    pub short_trader_long_mint_account: Box<Account<'info, TokenAccount>>,

    /// CHECK: unsafe
    #[account(
      associated_token::mint = short_mint_account,
      associated_token::authority = short_trader_mint_pda.key,
      mut
    )]
    pub short_trader_short_mint_account: Box<Account<'info, TokenAccount>>,

    ///CHECK: not doing 
    #[account(
      mut,
      seeds = [
        oracle_asset_key.as_ref(),
        &expiry.to_le_bytes(),
        &strike.to_le_bytes(),
        &strike_exponent.to_le_bytes(),
        b"vault"
    ], 
     bump,
    )]
    pub vault_account: AccountInfo<'info>,

    #[account(
      seeds = [
        oracle_asset_key.as_ref(),
        &expiry.to_le_bytes(),
        &strike.to_le_bytes(),
        &strike_exponent.to_le_bytes(),
        b"long"
      ], 
      bump,
      mut
    )]
    pub long_mint_account: Box<Account<'info, Mint>>,
    
    #[account(
      seeds = [
        oracle_asset_key.as_ref(),
        &expiry.to_le_bytes(),
        &strike.to_le_bytes(),
        &strike_exponent.to_le_bytes(),
        b"short"
      ], 
      bump,
      mut
    )]
    pub short_mint_account: Box<Account<'info, Mint>>,

    pub token_program: Program<'info, Token>,
    pub system_program: Program<'info, System>,
}

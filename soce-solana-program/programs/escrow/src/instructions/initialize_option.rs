use anchor_lang::prelude::*;
use anchor_spl::token::{Token, Mint};
//use std::{fmt, cmp::Ordering};
use core::mem::size_of;
//use std::collections::BinaryHeap;
use crate::errors::ErrorCode;

pub fn handler(
    ctx: Context<InitializeOption>,
    oracle_asset_key: Pubkey, // Needs validating i.e. if this is a real pyth oracle asset
    expiry: u64,
    strike: u64,
    strike_exponent: i32,
    oa_bump: u8,
    vault_bump: u8,
    long_mint_bump: u8,
    short_mint_bump: u8,
) -> ProgramResult {
    msg!("---------BEGIN: initialize_option---------");

    
    
    ctx.accounts.option_account.option_lister = *ctx.accounts.initializer.key;
    ctx.accounts.option_account.oracle_asset_key = oracle_asset_key;
    ctx.accounts.option_account.strike = strike;
    ctx.accounts.option_account.strike_exponent = strike_exponent;
    ctx.accounts.option_account.expiry = expiry;
    ctx.accounts.option_account.vault_account = ctx.accounts.vault_account.key();    
    ctx.accounts.option_account.short_mint = ctx.accounts.short_mint_account.key();
    ctx.accounts.option_account.long_mint = ctx.accounts.long_mint_account.key();
    ctx.accounts.option_account.oa_bump = oa_bump;
    ctx.accounts.option_account.vault_bump = vault_bump;
    ctx.accounts.option_account.long_mint_bump = long_mint_bump;
    ctx.accounts.option_account.short_mint_bump = short_mint_bump;

    //for stocks UTC should end with at 16:00 hrs EST - https://www.unixtimestamp.com/
    let now = Clock::get()?.unix_timestamp as u64;
    if ctx.accounts.option_account.expiry < now  {
        msg!("***Expiry in Past. Overriding this error for tests**");
        //return Err(ErrorCode::ExpiryInThePast.into());
    }
    ctx.accounts.option_account.is_settled = false;
    ctx.accounts.option_account.is_tie = false;
    ctx.accounts.option_account.winning_mint = oracle_asset_key; // Init with oracle asset key for sanity checks later.
    
    // OptionAccount::emit_option_account(&ctx.accounts.option_account);
    OptionAccount::log_option_account(&ctx.accounts.option_account);

    msg!("---------END: initialize_option-----------");
    Ok(())
}

#[account]
#[derive(Debug)]
pub struct OptionAccount {
    pub option_lister: Pubkey,
    pub oracle_asset_key: Pubkey,
    pub vault_account: Pubkey,
    pub long_mint: Pubkey,
    pub short_mint: Pubkey,
    pub expiry: u64,
    pub strike: u64,
    pub strike_exponent: i32,
    pub oa_bump:u8,
    pub vault_bump:u8,
    pub long_mint_bump:u8,
    pub short_mint_bump:u8,
    pub long_orderbook: Vec<OrderInfo>,
    pub short_orderbook: Vec<OrderInfo>,
    pub is_settled: bool,
    pub winning_mint: Pubkey,
    pub is_tie: bool
    //pub long_orderbook_heap: BinaryHeap<OrderInfo>,
   
}

// #[derive(AnchorSerialize, AnchorDeserialize, Debug)]
// pub struct OptionAccountType {
//     pub option_lister: Pubkey,
//     pub oracle_asset_key: Pubkey,
//     pub vault_account: Pubkey,
//     pub long_mint: Pubkey,
//     pub short_mint: Pubkey,
//     pub expiry: u64,
//     pub strike: u64,
//     pub strike_exponent: i32,
//     pub oa_bump:u8,
//     pub vault_bump:u8,
//     pub long_mint_bump:u8,
//     pub short_mint_bump:u8,
//     pub long_orderbook: Vec<OrderInfo>,
//     pub short_orderbook: Vec<OrderInfo>,
//     pub trader_long_amount: u64,
//     pub trader_short_amount: u64
// }

#[derive(AnchorSerialize, AnchorDeserialize, Copy, Debug)]
pub struct OrderInfo {
    pub trader: Pubkey,
    pub trader_mint_bump: u8,
    pub trader_long_mint_account: Pubkey,
    pub trader_short_mint_account: Pubkey,
    pub quantity: u64,
    pub price: u64,
    pub is_long: bool,
    pub is_close: bool,
    pub order_time: u64
}

impl Clone for OrderInfo {
    fn clone(&self) -> Self {
        unimplemented!()
    }
}
impl OrderInfo {
    pub const LEN: usize = size_of::<OrderInfo>();
}
// #[event]
// pub struct OrderBookEvent {
//     pub option_account: OptionAccountType
// }

impl OptionAccount {
    pub const LEN: usize = size_of::<OptionAccount>();

    pub fn log_option_account(&self) {
        msg!("OptionAccount::log_option_account:");
        msg!("{:?}", self);
        msg!("OptionAccount size: {:?}", size_of::<OptionAccount>());
        msg!("OrderInfo size: {:?}", size_of::<OrderInfo>());
    }
    pub fn emit_option_account(&self) {
        // emit!(OptionCreateEvent{
        //     option_account: self
        // });
    }
    pub fn log_long_orderbook(&self) {
        msg!("OptionAccount::log_long_orderbook: {:?}", self.long_orderbook)
    }
    pub fn log_short_orderbook(&self) {
        msg!("OptionAccount::log_short_orderbook: {:?}", self.short_orderbook)
    }
}

#[derive(Accounts)]
#[instruction(
    oracle_asset_key: Pubkey,
    expiry: u64,
    strike: u64,
    strike_exponent: i32,
    oa_bump:u8,
    vault_bump: u8,
    long_mint_bump: u8,
    short_mint_bump: u8,
)]
pub struct InitializeOption<'info> {

    /// CHECK: The initializer is fee payer. Not sure what to check here
    #[account(
        signer,
        mut
    )]
    pub initializer: AccountInfo<'info>,

    #[account(
        init,
        payer = initializer,
        //4 is for one vec len and each vec can have 15 . Well sum is 30
        space = 8 + OptionAccount::LEN + 4 + 15*(OrderInfo::LEN) + 4 + 15*(OrderInfo::LEN),
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"main"
        ], 
        bump
    )]
    pub option_account: Account<'info, OptionAccount>,

    /// CHECK: We are checking here for few things like seeds
    #[account(
        init,
        payer = initializer,
        space = 16,
        // mint::decimals = 9,
        // mint::authority = vault_account,
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"vault"
        ], 
        bump
    )]
    pub vault_account: AccountInfo<'info>,
   // pub vault_account: Account<'info, Mint>,

    #[account(
        init,
        payer = initializer,
        mint::decimals = 9,
        mint::authority = long_mint_account,
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"long"
        ], 
        bump
    )]
    pub long_mint_account: Account<'info, Mint>,

    #[account(
        init,
        payer = initializer,
        mint::decimals = 9,
        mint::authority = short_mint_account,
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"short"
        ], 
        bump
    )]
    pub short_mint_account: Account<'info, Mint>,
    pub token_program: Program<'info, Token>,
    pub rent: Sysvar<'info, Rent>,
    pub system_program: Program<'info, System>
}

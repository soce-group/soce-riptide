//use std::borrow::BorrowMut;
//use crate::accounts;
use crate::constants;
use crate::helper_functions;

use crate::errors::ErrorCode;
use crate::OptionAccount;
// use crate::OptionAccountType;
use crate::OrderInfo;
// use crate::OrderBookEvent;
//use std::collections::BinaryHeap;
use anchor_lang::prelude::*;
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::system_instruction;
use anchor_spl::associated_token::AssociatedToken;
use anchor_spl::token::{
    self,
    Burn,
    Mint,
    MintTo,
    SetAuthority,
    Token,
    TokenAccount,
    Transfer
};
use anchor_lang::Id;


// Needs validating against oa
// Option object for underlying
// true for long, false for short. Money won if price > strike
pub fn handler(
    ctx: Context<CollectOption>,
    oracle_asset_key: Pubkey,
    expiry: u64,
    strike: u64,
    strike_exponent: i32, 
    amount: u64,
    long_mint_bump: u8,
    short_mint_bump: u8,
    oa_bump: u8,
    vault_bump: u8,
    trader_mint_bump: u8,
    
) -> ProgramResult {
    msg!("---------BEGIN: collect_option---------");

    if ctx.accounts.option_account.is_settled{
        msg!("***NOT Settled. **");
        return Err(ErrorCode::OptionNotSettled.into());
    }
    if ctx.accounts.option_account.short_mint != ctx.accounts.short_mint_account.key() || 
       ctx.accounts.option_account.long_mint != ctx.accounts.long_mint_account.key()
    {
        return Err(ErrorCode::OptionMintMismatch.into());
    }

    let mut amount_to_refund: u64 = 0;

    if ctx.accounts.option_account.winning_mint == ctx.accounts.option_account.short_mint {
        amount_to_refund = ctx.accounts.trader_short_mint_account.amount * 100 * (10^constants::PRICE_EXPONENT as u64);
    }
    else if 
    ctx.accounts.option_account.winning_mint == ctx.accounts.option_account.long_mint {
        amount_to_refund = ctx.accounts.trader_long_mint_account.amount * 100 * (10^constants::PRICE_EXPONENT as u64);
    }
    else if ctx.accounts.option_account.winning_mint == ctx.accounts.option_account.oracle_asset_key && ctx.accounts.option_account.is_tie{
        amount_to_refund = (ctx.accounts.trader_short_mint_account.amount + ctx.accounts.trader_long_mint_account.amount) * 50 * (10^constants::PRICE_EXPONENT as u64);
    }

    //Burning tokens
    let trader_mint_seeds = [
    ctx.accounts.trader.key.as_ref(),
    &[trader_mint_bump]
  ];


    let burn_short_token = token::Burn {
        mint: ctx.accounts.short_mint_account.to_account_info(),
        to: ctx.accounts.trader_short_mint_account.to_account_info(),
        authority: ctx.accounts.trader_mint_pda.to_account_info(),
    };
    let burn_long_token = token::Burn {
        mint: ctx.accounts.long_mint_account.to_account_info(),
        to: ctx.accounts.trader_long_mint_account.to_account_info(),
        authority: ctx.accounts.trader_mint_pda.to_account_info(),
    };

    if ctx.accounts.trader_long_mint_account.amount > 0{

        let burn_long_result = token::burn(
            CpiContext::new_with_signer(
            ctx.accounts.token_program.to_account_info(),
            burn_long_token,
            &[&trader_mint_seeds],
            ),
            ctx.accounts.trader_long_mint_account.amount,
        );
    }
    if ctx.accounts.trader_short_mint_account.amount > 0 {

        let burn_long_result = token::burn(
            CpiContext::new_with_signer(
            ctx.accounts.token_program.to_account_info(),
            burn_short_token,
            &[&trader_mint_seeds],
            ),
            ctx.accounts.trader_short_mint_account.amount,
        );
    }
    
    let transfer_result = helper_functions::transfer_from_our_pda(
        &mut ctx.accounts.vault_account.to_account_info(),
        &mut ctx.accounts.trader.to_account_info(),
        amount_to_refund
    );
    

    msg!("---------END: collect_option-----------");
    Ok(())
}

#[derive(Accounts)]
#[instruction(
    oracle_asset_key: Pubkey,
    expiry: u64,
    strike: u64,
    strike_exponent: i32, 
    amount: u64,
    long_mint_bump: u8,
    short_mint_bump: u8,
    oa_bump: u8,
    vault_bump: u8,
    trader_mint_bump: u8,
    
)]


pub struct CollectOption<'info> {

     /// CHECK: Again wallet is signer and we check its key against our pda
    #[account(
        signer,
        mut
    )]
    pub trader: AccountInfo<'info>,

    /// CHECK: We check if seeds match
    #[account(
        mut,
        seeds = [ trader.key.as_ref() ],
        bump
    )]
    pub trader_mint_pda: AccountInfo<'info>,

    #[account(
        mut,
        associated_token::mint = long_mint_account,
        associated_token::authority = trader_mint_pda,
    )]
    pub trader_long_mint_account: Box<Account<'info, TokenAccount>>,

    #[account(
        mut,
        associated_token::mint = short_mint_account,
        associated_token::authority = trader_mint_pda,
    )]
    pub trader_short_mint_account: Box<Account<'info, TokenAccount>>,

    #[account(
        mut
    )]
    pub option_account: Box<Account<'info, OptionAccount>>,

    ///CHECK: HAve to pass seeds
    #[account(mut)]
    pub vault_account: AccountInfo<'info>,
    //Check if mints match
    #[account(mut)]
    pub long_mint_account: Box<Account<'info, Mint>>,
    #[account(mut)]
    pub short_mint_account: Box<Account<'info, Mint>>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub rent: Sysvar<'info, Rent>,
    pub system_program: Program<'info, System>,
}

//use std::borrow::BorrowMut;
use crate::constants;
//use crate::accounts;
use crate::errors::ErrorCode;
use crate::OptionAccount;
// use crate::OptionAccountType;
use crate::OrderInfo;
// use crate::OrderBookEvent;
//use std::collections::BinaryHeap;
use anchor_lang::prelude::*;
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::system_instruction;
use anchor_spl::associated_token::AssociatedToken;
use anchor_spl::token::{
    self,
    Burn,
    Mint,
    MintTo,
    SetAuthority,
    Token,
    TokenAccount,
    Transfer
};
use anchor_lang::Id;


// Needs validating against oa
// Option object for underlying
// true for long, false for short. Money won if price > strike
pub fn handler(
    ctx: Context<OpenNewTrade>,
    oracle_asset_key: Pubkey, 
    option_account: Pubkey,
    oa_bump: u8,
    strike: u64,
    strike_exponent: i32,
    expiry: u64,
    quantity: u64,
    price: u64,
    is_long: bool,
    is_close : bool,
    trader_mint_bump: u8
) -> ProgramResult {
    msg!("---------BEGIN: open_new_trade---------");

    assert!(ctx.accounts.option_account.oracle_asset_key == oracle_asset_key);
    if ctx.accounts.option_account.is_settled{
        return Err(ErrorCode::OptionSettled.into());
    }

    if price > 99 || price < 1 {
        return Err(ErrorCode::PriceError.into());
    }

    
    let order_time = Clock::get()?.unix_timestamp as u64;
    let mut order_price =price;
    let mut order_long =is_long;
    
    //Fees to be added later
    //TODO
    //let fees = quantity * price * (10^(constants::PRICE_EXPONENT - 2) as u64);
    //Start with nominal fees to cover money txn cost
    let fees = 1000; 
    let mut amount: u64 = quantity * price * (10^constants::PRICE_EXPONENT as u64);
    

   
    // This is not needed as the one in is_close if block takes care of this
    //But that is expensive computation so better to do this check IMO
    if is_close{
         //Let us make sure you have position you want to close
         msg!("*******HERE****---------");
        if is_long && ctx.accounts.trader_long_mint_account.amount < quantity {
            return Err(ErrorCode::CloseTradeError.into());
        } 
        if !is_long && ctx.accounts.trader_short_mint_account.amount < quantity {
            return Err(ErrorCode::CloseTradeError.into());
        } 
    }

    let mut close_order_in_book :u64 = 0;
    // Check if the trade is closing and then check if the trader has existing position (owns the token)
    //IF she does then only take the money for new position (if any) -- SCRATCH THIS. For this will be in V2. Automatic tracking and closing 
    // Make sure closing trade quantity is less than mint quantity.
    if is_close {
        order_price = 100 - price;
        order_long = !is_long;
        //If closing existing long Position (long mint has tokens)
        // is_long flag is set to long for a existing long position. 
        //That is the convention. We change it to short in OrderInfo by using order_long boolean
        //Same with price. Trader just says the price he closes at. we doe 100-price and put that order on ob

        // Check if there is a existing short order already that is closing any long
        if is_long && ctx.accounts.trader_long_mint_account.amount > 0 {
            for i in ctx.accounts.option_account.short_orderbook.iter(){
                if i.trader.key() == ctx.accounts.trader.key() && i.is_close {
                    close_order_in_book += i.quantity;
                }
            }
        }
        if !is_long && ctx.accounts.trader_short_mint_account.amount > 0{
            for i in ctx.accounts.option_account.long_orderbook.iter(){
                if i.trader.key() == ctx.accounts.trader.key() && i.is_close{
                    close_order_in_book += i.quantity;
                }
            }
        }
        //Let us make sure you have position you want to close  after taking close orders in OB
        if is_long && ctx.accounts.trader_long_mint_account.amount < (quantity + close_order_in_book) {
            return Err(ErrorCode::CloseTradeError.into());
        } 
        if !is_long && ctx.accounts.trader_short_mint_account.amount < (quantity + close_order_in_book) {
            return Err(ErrorCode::CloseTradeError.into());
        } 
        //If all rules pass it is a closing position so don't charge any money
        amount = 0;
    }
    

    let new_order : OrderInfo = OrderInfo {
        trader: ctx.accounts.trader.key(),
        trader_mint_bump: trader_mint_bump,
        trader_long_mint_account: ctx.accounts.trader_long_mint_account.key(),
        trader_short_mint_account: ctx.accounts.trader_short_mint_account.key(),
        quantity: quantity,
        price: order_price,
        is_long: order_long,
        is_close: is_close,
        order_time: order_time
    };

    // let long_mint_seeds = [
    //     oracle_asset_key.as_ref(),
    //     &expiry.to_le_bytes(),
    //     &strike.to_le_bytes(),
    //     &strike_exponent.to_le_bytes(),
    //     b"long",
    //     &[ctx.accounts.option_account.long_mint_bump]
    // ]; 

    // let short_mint_seeds = [
    //     oracle_asset_key.as_ref(),
    //     &expiry.to_le_bytes(),
    //     &strike.to_le_bytes(),
    //     &strike_exponent.to_le_bytes(),
    //     b"short",
    //     &[ctx.accounts.option_account.short_mint_bump]
    // ];



    let ix = system_instruction::transfer(
        ctx.accounts.trader.key,
        ctx.accounts.vault_account.to_account_info().key,
        amount + fees,
    );

    // all accounts in instruction ix must be included in account_infos
    invoke(
        &ix,
        &[
            ctx.accounts.trader.to_account_info(),
            ctx.accounts.vault_account.to_account_info(),
            ctx.accounts.system_program.to_account_info(),
        ],
    )?;
  
    // Money has been taken and entry will be added to the orderbook
    // Front-end will receive the emitted OptionAccount and parse the orderbook for a match
    // Front-end will then pass the matched accounts to MatchOrder
    // ctx.accounts.option_account.long_orderbook.push(fake_long);
    // ctx.accounts.option_account.short_orderbook.push(fake_short);
    if is_long{
        ctx.accounts.option_account.long_orderbook.push(new_order);
    }
    else{
        ctx.accounts.option_account.short_orderbook.push(new_order);
    }

    // add match code and emit the match only
    // replace emit with cpi against new program
    // emit!(OrderBookEvent{
    //     option_account: OptionAccountType {
    //         option_lister: ctx.accounts.option_account.option_lister,
    //         oracle_asset_key: ctx.accounts.option_account.oracle_asset_key,
    //         vault_account: ctx.accounts.option_account.vault_account,
    //         long_mint: ctx.accounts.option_account.long_mint,
    //         short_mint: ctx.accounts.option_account.short_mint,
    //         expiry: ctx.accounts.option_account.expiry,
    //         strike: ctx.accounts.option_account.strike,
    //         strike_exponent: ctx.accounts.option_account.strike_exponent,
    //         oa_bump: ctx.accounts.option_account.oa_bump,
    //         vault_bump: ctx.accounts.option_account.vault_bump,
    //         long_mint_bump: ctx.accounts.option_account.long_mint_bump,
    //         short_mint_bump: ctx.accounts.option_account.short_mint_bump,
    //         long_orderbook: ctx.accounts.option_account.long_orderbook.to_vec(),
    //         short_orderbook: ctx.accounts.option_account.short_orderbook.to_vec(),
    //         trader_long_amount: ctx.accounts.trader_long_mint_account.amount,
    //         trader_short_amount: ctx.accounts.trader_short_mint_account.amount
    //     }
    // });
    msg!("---------END: open_new_trade-----------");
    Ok(())
}

#[derive(Accounts)]
#[instruction(
    oracle_asset_key: Pubkey,
    expiry: u64,
    strike: u64,
    strike_exponent: i32,
    trader_mint_bump: u8,
    oa_bump: u8
)]
pub struct OpenNewTrade<'info> {

    /// CHECK: unsafe
    #[account(
        signer,
        mut
    )]
    pub trader: AccountInfo<'info>,

    /// CHECK: unsafe
    #[account(
        init_if_needed,
        payer = trader,
        space = 16,
        seeds = [ trader.key.as_ref() ],
        bump
    )]
    pub trader_mint_pda: AccountInfo<'info>,

    #[account(
        init_if_needed,
        payer = trader,
        associated_token::mint = long_mint_account,
        associated_token::authority = trader_mint_pda,
    )]
    pub trader_long_mint_account: Box<Account<'info, TokenAccount>>,

    #[account(
        init_if_needed,
        payer = trader,
        associated_token::mint = short_mint_account,
        associated_token::authority = trader_mint_pda,
    )]
    pub trader_short_mint_account: Box<Account<'info, TokenAccount>>,


    #[account(
        mut
    )]
    pub option_account: Box<Account<'info, OptionAccount>>,

    /// CHECK: unsafe
    #[account(mut)]
    //pub vault_account: Box<Account<'info, Mint>>,
    pub vault_account: AccountInfo<'info>,
    //Check if mints match
    #[account(mut)]
    pub long_mint_account: Box<Account<'info, Mint>>,
    #[account(mut)]
    pub short_mint_account: Box<Account<'info, Mint>>,
    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub rent: Sysvar<'info, Rent>,
    pub system_program: Program<'info, System>,
}

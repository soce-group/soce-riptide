use std::sync::Arc;
use anchor_lang::prelude::*;
use anchor_spl::token::{Token, Mint};
use crate::OptionAccount;
use crate::errors::ErrorCode;
use pyth_client;

pub fn handler(
    ctx: Context<SettleOption>,
    oracle_asset_key: Pubkey, // Needs validating against oa
    oa_bump: u8,
    strike: u64,
    strike_exponent: i32,
    expiry: u64,
    long_mint_bump: u8,
    short_mint_bump: u8,
) -> ProgramResult {
    msg!("---------BEGIN: settle_option---------");
    //Error: Settled already
    if ctx.accounts.option_account.is_settled{
        msg!("***Settled in the past. **");
        return Err(ErrorCode::OptionSettled.into());
    }
    let now = Clock::get()?.unix_timestamp as u64;
    //Error: 
    if ctx.accounts.option_account.expiry > now  {
        msg!("***Expiry in Future. **");
        //TODO :Uncomment  in prd
        // return Err(ErrorCode::ExpiryInTheFuture.into());
    }
    //Error. Settled already but flag not turned. Or someone is trying some shenanigans
    if ctx.accounts.option_account.winning_mint.key() != ctx.accounts.option_account.oracle_asset_key{
        msg!("***Settled in the past. **");
        return Err(ErrorCode::OptionSettled.into());
    }

    let pyth_product_info =  ctx.accounts.oracle_asset_info.to_account_info();
    let pyth_product_data =  pyth_product_info.try_borrow_data()?;
    //let product_account = pyth_client::load_product( &pyth_product_data ).unwrap();
    let pyth_product = pyth_client::load_product(&pyth_product_data).unwrap();
    //let pyth_product = pyth_client::cast::<pyth_client::Product>(pyth_product_data);

    if pyth_product.magic != pyth_client::MAGIC {
        msg!("Pyth product account provided is not a valid Pyth account");
        return Err(ProgramError::InvalidArgument.into());
    }
    if pyth_product.atype != pyth_client::AccountType::Product as u32 {
        msg!("Pyth product account provided is not a valid Pyth product account");
        return Err(ProgramError::InvalidArgument.into());
    }
    if pyth_product.ver != pyth_client::VERSION_2 {
        msg!("Pyth product account provided has a different version than the Pyth client");
        return Err(ProgramError::InvalidArgument.into());
    }
    if !pyth_product.px_acc.is_valid() {
        msg!("Pyth product price account is invalid");
        return Err(ProgramError::InvalidArgument.into());
    }
    // if binary_option.underlying_asset_address != *underlying_asset_info.key {
    //     msg!("Pyth product account account is not same as option");
    //     return Err(BinaryOptionError::UnderlyingAssetInvalid.into());
    // }

    // let pyth_price_pubkey = Pubkey::new(&pyth_product.px_acc.val);
    // if &pyth_price_pubkey != underlying_asset_px_info.key {
    //     msg!("Pyth product price account does not match the Pyth price provided");
    //     return Err(ProgramError::InvalidArgument.into());
    // }

    let pyth_price_info =  ctx.accounts.oracle_asset_price_info.to_account_info();
    let pyth_price_data = pyth_price_info.try_borrow_data()?;
    let pyth_price = pyth_client::load_price(&pyth_price_data).unwrap();
    msg!("    price ........ {}", pyth_price.agg.price);
    msg!("    conf ......... {}", pyth_price.agg.conf);
    msg!("    valid_slot ... {}", pyth_price.valid_slot);
    msg!("    publish_slot . {}", pyth_price.agg.pub_slot);

    // //msg!(&pyth_price_pubkey.to_string());

    let settle_price = pyth_price.agg.price as f64;
    let base10int = 10 as i64;
    let base10 = 10.0 as f64;
    let confidence_interval =  pyth_price.agg.conf as f64;
    let pyth_expo_factor:f64 =  if pyth_price.expo < 0 {1.0/base10int.pow(-pyth_price.expo as u32) as f64} else {base10int.pow(pyth_price.expo as u32) as f64};
    let bo_strike = ctx.accounts.option_account.strike as f64;
    let bo_exp = ctx.accounts.option_account.strike_exponent as i32;
    let final_settle_px = settle_price*pyth_expo_factor;
    let final_conf = confidence_interval*pyth_expo_factor;
    let strike_px = bo_strike*(base10.powi(bo_exp));

    if (final_settle_px + final_conf) > strike_px {
        ctx.accounts.option_account.winning_mint = ctx.accounts.option_account.long_mint;
    }
    else if (final_settle_px - final_conf) < strike_px {
        ctx.accounts.option_account.winning_mint = ctx.accounts.option_account.short_mint;
    }
    //This is if we dont know whether long one or short. SO divide equally for now.
    // maybe implementation will be in future
    else {
        ctx.accounts.option_account.is_tie = true;
    }
    ctx.accounts.option_account.is_settled = true;

    msg!("---------END: settle_option-----------");
    Ok(())
}

#[derive(Accounts)]
#[instruction(
    oracle_asset_key: Pubkey, // Needs validating against oa
    oa_bump: u8,
    strike: u64,
    strike_exponent: i32,
    expiry: u64,
    long_mint_bump: u8,
    short_mint_bump: u8,
)]
pub struct SettleOption<'info> {

    /// CHECK: unsafe
    #[account(
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"main"
        ], 
        bump,
        mut
    )]
    pub option_account: Account<'info, OptionAccount>,

    #[account(
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"long"
        ], 
        bump
    )]
    pub long_mint_account: Account<'info, Mint>,
    #[account(
        seeds = [
            oracle_asset_key.as_ref(),
            &strike.to_le_bytes(),
            &expiry.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"short"
        ], 
        bump
    )]
    pub short_mint_account: Account<'info, Mint>,
    ///CHECK: In code
    pub oracle_asset_info: AccountInfo<'info>,
    ///CHECK: In code
    pub oracle_asset_price_info: AccountInfo<'info>,
    
    pub token_program: Program<'info, Token>,
    pub rent: Sysvar<'info, Rent>,
    pub system_program: Program<'info, System>,
}
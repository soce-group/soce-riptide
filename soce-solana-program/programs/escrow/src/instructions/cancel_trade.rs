use crate::constants;
use crate::helper_functions;
use crate::errors::ErrorCode;
use crate::OptionAccount;
// use crate::OptionAccountType;
use crate::OrderInfo;
// use crate::OrderBookEvent;
//use std::collections::BinaryHeap;
use anchor_lang::prelude::*;
use anchor_lang::solana_program::program::invoke;
use anchor_lang::solana_program::program::invoke_signed;
use anchor_lang::solana_program::system_instruction;
use anchor_spl::associated_token::AssociatedToken;
use anchor_spl::token::{
    self,
    Burn,
    Mint,
    MintTo,
    SetAuthority,
    Token,
    TokenAccount,
    Transfer
};
use anchor_lang::Id;


// Needs validating against oa
// Option object for underlying
// true for long, false for short. Money won if price > strike
pub fn handler(
    ctx: Context<CancelTrade>,
    oracle_asset_key: Pubkey,
    option_account: Pubkey,
    oa_bump: u8,
    strike: u64,
    strike_exponent: i32,
    expiry: u64,
    quantity: u64,
    price: u64,
    is_long: bool,
    is_close : bool,
    vault_bump:u8
) -> ProgramResult {
    msg!("---------BEGIN: cancel_trade---------");

    assert!(ctx.accounts.option_account.oracle_asset_key == oracle_asset_key);
    if ctx.accounts.option_account.is_settled{
        return Err(ErrorCode::OptionSettled.into());
    }

    if price > 99 || price < 1 {
        return Err(ErrorCode::PriceError.into());
    }

    let mut amount_to_refund: u64 = 0;
    let index:usize;
   
    if is_long{

        let result = ctx.accounts.option_account.long_orderbook.iter().position
            (|&x| 
                x.trader == ctx.accounts.trader.key() && 
                x.quantity == quantity && 
                x.is_long == is_long &&
                x.is_close == is_close &&
                x.price == price
        );

        match result {
            // There was an order
            Some(x) => index = x,
            // The division was invalid
            None    => return Err(ErrorCode::CancelOrderDetailsError.into()),
        }

        if !is_close{
            amount_to_refund = quantity * price * (10^constants::PRICE_EXPONENT as u64);
            helper_functions::transfer_from_our_pda(
                &mut ctx.accounts.vault_account.to_account_info(),
                &mut ctx.accounts.trader.to_account_info(),
                amount_to_refund
            )?;
        }

        //Remove trade from OB
       ctx.accounts.option_account.long_orderbook.remove(index);
    }
    //IF SHORT
    else{
        let result = ctx.accounts.option_account.short_orderbook.iter().position
            (|&x| 
                x.trader == ctx.accounts.trader.key() && 
                x.quantity == quantity && 
                x.is_long == is_long &&
                x.is_close == is_close &&
                x.price == price
        );
        match result {
            // There was an order
            Some(x) => index = x,
            // The division was invalid
            None    => return Err(ErrorCode::CancelOrderDetailsError.into()),
        }
        if !is_close{
            amount_to_refund = quantity * price * (10^constants::PRICE_EXPONENT as u64);
            helper_functions::transfer_from_our_pda(
                &mut ctx.accounts.vault_account.to_account_info(),
                &mut ctx.accounts.trader.to_account_info(),
                amount_to_refund
            )?;
        }

        //Remove trade from OB
        ctx.accounts.option_account.short_orderbook.remove(index);
    }
    msg!("---------END: cancel_trade-----------");
    Ok(())
}

#[derive(Accounts)]
#[instruction(
    oracle_asset_key: Pubkey,
    option_account: Pubkey,
    oa_bump: u8,
    strike: u64,
    strike_exponent: i32,
    expiry: u64,
    quantity: u64,
    price: u64,
    is_long: bool,
    is_close : bool,
    vault_bump:u8
)]


pub struct CancelTrade<'info> {

    /// CHECK: Trader is the signer
    #[account(
        signer,
        mut
    )]
    pub trader: AccountInfo<'info>,

    #[account(
        mut,
    )]
    pub option_account: Box<Account<'info, OptionAccount>>,

    ///CHECK:Cheking for seeds
    #[account(
        seeds = [
            oracle_asset_key.as_ref(),
            &expiry.to_le_bytes(),
            &strike.to_le_bytes(),
            &strike_exponent.to_le_bytes(),
            b"vault"
        ], 
       bump,
       mut
    )]
    pub vault_account: AccountInfo<'info>,

    pub associated_token_program: Program<'info, AssociatedToken>,
    pub token_program: Program<'info, Token>,
    pub rent: Sysvar<'info, Rent>,
    pub system_program: Program<'info, System>,
}

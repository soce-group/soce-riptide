import * as anchor from '@project-serum/anchor';
import { Program, BN, IdlAccounts, AccountNamespace, Accounts } from '@project-serum/anchor';
import { PublicKey, Keypair, SystemProgram, SYSVAR_RENT_PUBKEY } from "@solana/web3.js";
import { TOKEN_PROGRAM_ID, Token, ASSOCIATED_TOKEN_PROGRAM_ID, AccountLayout } from "@solana/spl-token";
import { nu64, s32 } from "@solana/buffer-layout";
import { assert } from "chai";
import { Escrow } from '../target/types/escrow';
import { token } from '@project-serum/anchor/dist/cjs/utils';

describe('escrow', async () => {
  console.log('describe escrow')
  // console.log('anchor: ', anchor)
  
  // console.log('anchor.Provider.env():', anchor.Provider.env())
  const provider = anchor.Provider.env();
  anchor.setProvider(provider)
  
  // console.log('anchor.workspace.Escrow:', anchor.workspace.Escrow)
  const program = anchor.workspace.Escrow as Program<Escrow>;

  // console.log(anchor.workspace)

  const fake_trader = new Keypair()
  const oracle_asset_key = new PublicKey("DDdPuysfkxPq5Y1ZtTSk1H5n7iBKc9wtEKUwd1TNu3Gc")
  const expiry = new BN(76000)
  const strike = new BN(76000)
  const strike_exponent = new BN(-8)
  const oa_seeds = [
    oracle_asset_key.toBuffer(),
    expiry.toBuffer('le',8),
    strike.toBuffer('le',8),
    strike_exponent.toTwos(32).toBuffer('le',4),
    Buffer.from("main")
  ]
  const vault_seeds = [
    oracle_asset_key.toBuffer(),
    expiry.toBuffer('le',8),
    strike.toBuffer('le',8),
    strike_exponent.toTwos(32).toBuffer('le',4),
    Buffer.from("vault")
  ]
  const long_mint_seeds = [
    oracle_asset_key.toBuffer(),
    expiry.toBuffer('le',8),
    strike.toBuffer('le',8),
    strike_exponent.toTwos(32).toBuffer('le',4),
    Buffer.from("long")
  ]
  const short_mint_seeds = [
    oracle_asset_key.toBuffer(),
    expiry.toBuffer('le',8),
    strike.toBuffer('le',8),
    strike_exponent.toTwos(32).toBuffer('le',4),
    Buffer.from("short")
  ]
  const [oa_pda, oa_bump] = await PublicKey.findProgramAddress(
    oa_seeds,
    program.programId
  );
  const [vault_pda, vault_bump] = await PublicKey.findProgramAddress(
    vault_seeds,
    program.programId
  );
  const [long_mint_pda, long_mint_bump] = await PublicKey.findProgramAddress(
    long_mint_seeds,
    program.programId
  );
  const [short_mint_pda, short_mint_bump] = await PublicKey.findProgramAddress(
    short_mint_seeds,
    program.programId
  );

  console.log("fake trader: ", fake_trader.publicKey.toBase58())
  console.log("oracle_asset_key: ", oracle_asset_key.toBase58())
  console.log("vault_pda: ", vault_pda.toBase58())
  console.log("oa_pda: ", oa_pda.toBase58())
  console.log("long_mint_pda : ", long_mint_pda.toBase58())
  console.log("short_mint_pda: ", short_mint_pda.toBase58())

  // TEST #1
  // Initialize exisiting escrow account
  it("Initialize existing escrow", async () => {
    console.log("test #1: initialize existing escrow")

    await provider.connection.confirmTransaction(
      await provider.connection.requestAirdrop(fake_trader.publicKey, 100000000000),
      "confirmed"
    );

    await program.rpc.initializeOption(
      oracle_asset_key,
      expiry,
      strike,
      strike_exponent,
      oa_bump,
      vault_bump,
      long_mint_bump,
      short_mint_bump,
      {
        accounts: {
          initializer: fake_trader.publicKey,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          longMintAccount: long_mint_pda,
          shortMintAccount: short_mint_pda,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [fake_trader],
      },
    )
  })

  it("OpenNewTrade and MatchOrder", async () => {
    console.log(
      "test #2: \n",
      "Opens four trades: \n",
      "(is_long, is_close)",
      "(is_long, is_close)",
      "(is_short, is_close)",
      "(is_short, is_close)"
    )
    const amount = new BN(10)
    const is_long = true as boolean
    const is_close = false as boolean


    const fake_long_trader = new Keypair();
    const fake_short_trader = new Keypair();

    const long_trader = fake_long_trader;
    const short_trader = fake_short_trader;

    await provider.connection.confirmTransaction(
      await provider.connection.requestAirdrop(long_trader.publicKey, 100000000000),
      "confirmed"
    );
    await provider.connection.confirmTransaction(
      await provider.connection.requestAirdrop(short_trader.publicKey, 100000000000),
      "confirmed"
    );

    const long_trader_mint_seeds = [
      long_trader.publicKey.toBuffer()
    ]
    const [ long_trader_mint_pda, long_trader_mint_bump ] = await PublicKey.findProgramAddress(
      long_trader_mint_seeds,
      program.programId
    );

    const long_trader_long_mint_pda = await token.associatedAddress({
      mint: long_mint_pda,
      owner: long_trader_mint_pda
    })
    const long_trader_short_mint_pda = await token.associatedAddress({
      mint: short_mint_pda,
      owner: long_trader_mint_pda
    })

    const short_trader_mint_seeds = [
      short_trader.publicKey.toBuffer()
    ]
    const [ short_trader_mint_pda, short_trader_mint_bump ] = await PublicKey.findProgramAddress(
      short_trader_mint_seeds,
      program.programId
    );

    const short_trader_long_mint_pda = await token.associatedAddress({
      mint: long_mint_pda,
      owner: short_trader_mint_pda
    })

    const short_trader_short_mint_pda = await token.associatedAddress({
      mint: short_mint_pda,
      owner: short_trader_mint_pda
    })


    console.log("amount: ", amount)
    console.log("is_long: ", is_long)
    console.log("is_close ", is_close)
    console.log("trader: ", long_trader.publicKey.toBase58(),)
    console.log("long_trader_mint_pda: ", long_trader_mint_pda.toBase58(),)
    console.log("longTraderLongMintAccount: ", long_trader_long_mint_pda.toBase58(),)
    console.log("longTraderShortMintAccount: ", long_trader_short_mint_pda.toBase58(),)
    console.log("short_trader_mint_pda: ", short_trader_mint_pda.toBase58(),)
    console.log("shortTraderLongMintAccount: ", short_trader_long_mint_pda.toBase58(),)
    console.log("shortTraderShortMintAccount: ", short_trader_short_mint_pda.toBase58(),)
    console.log("optionAccount: ", oa_pda.toBase58(),)
    console.log("vaultAccount: ", vault_pda.toBase58(),)
    console.log("longMintAccount: ", long_mint_pda.toBase58(),)
    console.log("shortMintAccount: ", short_mint_pda.toBase58(),)
    console.log("associatedTokenProgram: ", ASSOCIATED_TOKEN_PROGRAM_ID.toBase58(),)
    console.log("tokenProgram: ", TOKEN_PROGRAM_ID.toBase58(),)
    console.log("rent: ", SYSVAR_RENT_PUBKEY.toBase58(),)
    console.log("systemProgram: ", SystemProgram.programId.toBase58())
    console.log("program.programId: ", program.programId.toBase58())


    await program.rpc.openNewTrade(
      oracle_asset_key,
      oa_pda,
      oa_bump,
      strike,
      strike_exponent,
      expiry,
      amount,
      new BN(69),
      is_long,
      is_close,
      long_trader_mint_bump,
      {
        accounts: {
          trader: long_trader.publicKey,
          traderMintPda: long_trader_mint_pda,
          traderLongMintAccount: long_trader_long_mint_pda,
          traderShortMintAccount: long_trader_short_mint_pda,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          longMintAccount: long_mint_pda,
          shortMintAccount: short_mint_pda,
          associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [long_trader],
      },
    )

    await program.rpc.openNewTrade(
      oracle_asset_key,
      oa_pda,
      oa_bump,
      strike,
      strike_exponent,
      expiry,
      amount,
      new BN(99),
      !is_long,
      is_close,
      short_trader_mint_bump,
      {
        accounts: {
          trader: short_trader.publicKey,
          traderMintPda: short_trader_mint_pda,
          traderLongMintAccount: short_trader_long_mint_pda,
          traderShortMintAccount: short_trader_short_mint_pda,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          longMintAccount: long_mint_pda,
          shortMintAccount: short_mint_pda,
          associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [short_trader],
      },
    )

    await program.rpc.openNewTrade(
      oracle_asset_key,
      oa_pda,
      oa_bump,
      strike,
      strike_exponent,
      expiry,
      amount,
      new BN(12),
      is_long,
      is_close,
      long_trader_mint_bump,
      {
        accounts: {
          trader: long_trader.publicKey,
          traderMintPda: long_trader_mint_pda,
          traderLongMintAccount: long_trader_long_mint_pda,
          traderShortMintAccount: long_trader_short_mint_pda,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          longMintAccount: long_mint_pda,
          shortMintAccount: short_mint_pda,
          associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [long_trader],
      },
    )

    let zz = await program.rpc.openNewTrade(
      oracle_asset_key,
      oa_pda,
      oa_bump,
      strike,
      strike_exponent,
      expiry,
      amount,
      new BN(89),
      !is_long,
      is_close,
      short_trader_mint_bump,
      {
        accounts: {
          trader: short_trader.publicKey,
          traderMintPda: short_trader_mint_pda,
          traderLongMintAccount: short_trader_long_mint_pda,
          traderShortMintAccount: short_trader_short_mint_pda,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          longMintAccount: long_mint_pda,
          shortMintAccount: short_mint_pda,
          associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [short_trader],
      },
    )
    console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&");
    console.log(zz);

    let oa_layout = await program.account.optionAccount.fetch(
      oa_pda
    )

    console.log(oa_layout)

    console.log("****Before Cancel****************")
    console.log(vault_bump);
    await program.rpc.cancelTrade(
      oracle_asset_key,
      oa_pda,
      oa_bump,
      strike,
      strike_exponent,
      expiry,
      amount,
      new BN(12),
      is_long,
      is_close,
      vault_bump,
      {
        accounts: {
          trader: long_trader.publicKey,
          optionAccount: oa_pda,
          vaultAccount: vault_pda,
          associatedTokenProgram: ASSOCIATED_TOKEN_PROGRAM_ID,
          tokenProgram: TOKEN_PROGRAM_ID,
          rent: SYSVAR_RENT_PUBKEY,
          systemProgram: SystemProgram.programId
        },
        signers: [long_trader],
      },
    )

    //  oa_layout = program.coder.accounts.decode(
    //   "OptionAccount",
    //   oa_pda.    )

    console.log("AFTER CLOSE")
    oa_layout = await program.account.optionAccount.fetch(
      oa_pda
    )

    console.log(oa_layout)

    interface OrderInfo {
      trader: PublicKey,
      traderMintPda: PublicKey,
      traderLongMintAccount: PublicKey,
      traderShortMintAccount: PublicKey,
      quantity: BN,
      price: BN,
      isLong: Boolean,
      orderTime: BN
    }

    interface MatchedOrders {
      long: OrderInfo,
      short: OrderInfo,
    }

    let long_orderbook = oa_layout.longOrderbook as Array<OrderInfo>;
    let short_orderbook = oa_layout.shortOrderbook as Array<OrderInfo>;

    long_orderbook.sort((a, b) =>
      a.price.toNumber() - b.price.toNumber()
    );

    short_orderbook.sort((a, b) =>
      a.price.toNumber() - b.price.toNumber()
    );
    console.log("orderbooks before match")
    console.log("long_orderbook", long_orderbook)
    console.log("short_orderbook", short_orderbook)



    const top_long_orderbook = long_orderbook.pop()
    const top_short_orderbook = short_orderbook.pop()

    const matched_orders: MatchedOrders = {
      long: top_long_orderbook,
      short: top_short_orderbook
    }

    if ( top_long_orderbook.price.toNumber() 
        + top_short_orderbook.price.toNumber() > 99
    ) {
      console.log("found match")
            
      const matched_long_trader_mint_seeds = [
        matched_orders.long.trader.toBuffer()
      ]

      const matched_short_trader_mint_seeds = [
        matched_orders.short.trader.toBuffer()
      ]

      const [ 
        matched_long_trader_mint_pda,
        matched_long_trader_mint_bump
      ] = await PublicKey.findProgramAddress(
          matched_long_trader_mint_seeds,
          program.programId
      );
      
      const [
        matched_short_trader_mint_pda,
        matched_short_trader_mint_bump
      ] = await PublicKey.findProgramAddress(
          matched_short_trader_mint_seeds,
          program.programId
      );

      let matched_qty = new BN(0);
      if ( matched_orders.long.quantity > matched_orders.short.quantity ) {
        matched_qty = matched_orders.short.quantity;
      } else {
        matched_qty = matched_orders.long.quantity;
      };

      console.log("submitting matchOrder:")
      // console.log("long_orderbook", long_orderbook)
      // console.log("short_orderbook", short_orderbook)
      console.log("matched_qty.price", matched_qty.toNumber())
      console.log("top_long_orderbook.price", top_long_orderbook.price.toNumber())
      console.log("top_short_orderbook.price", top_short_orderbook.price.toNumber())
      console.log("matched_orders.long.trader: ", matched_orders.long.trader.toBase58())
      console.log("matched_orders.long.price: ", matched_orders.long.price.toNumber())
      console.log("matched_orders.long.quantity: ", matched_orders.long.quantity.toNumber())
      console.log("matched_orders.short.trader: ", matched_orders.short.trader.toBase58())
      console.log("matched_orders.short.price: ", matched_orders.short.price.toNumber())
      console.log("matched_orders.short.quantity: ", matched_orders.short.quantity.toNumber())

      await program.rpc.matchOrder(
        oracle_asset_key,
        long_mint_bump,
        short_mint_bump,
        expiry,
        strike,
        strike_exponent,
        matched_qty,
        matched_long_trader_mint_bump,
        matched_short_trader_mint_bump,
        vault_bump,
        {
          accounts: {
            optionAccount: oa_pda,
            longTrader: matched_orders.long.trader,
            longTraderMintPda: matched_long_trader_mint_pda,
            longTraderLongMintAccount: matched_orders.long.traderLongMintAccount,
            longTraderShortMintAccount: matched_orders.long.traderShortMintAccount,
            shortTrader: matched_orders.short.trader,
            shortTraderMintPda: matched_short_trader_mint_pda,
            shortTraderLongMintAccount: matched_orders.short.traderLongMintAccount,
            shortTraderShortMintAccount: matched_orders.short.traderShortMintAccount,
            vaultAccount: vault_pda,
            longMintAccount: long_mint_pda,
            shortMintAccount: short_mint_pda,
            tokenProgram: TOKEN_PROGRAM_ID,
            systemProgram: SystemProgram.programId
          },
          signers: [],
        },
      )

      // await program.rpc.settleOption(
      //   oracle_asset_key,
      //   oa_bump,
      //   strike,
      //   strike_exponent,
      //   expiry,
      //   long_mint_bump,
      //   short_mint_bump,
     
      //   {
      //     accounts: {
      //       optionAccount: oa_pda,
      //       longMintAccount: long_mint_pda,
      //       shortMintAccount: short_mint_pda,
      //       oracleAssetInfo: oracle_asset_key,
      //       oracleAssetPriceInfo: oracle_asset_key,
      //       tokenProgram: TOKEN_PROGRAM_ID,
      //       rent: SYSVAR_RENT_PUBKEY,
      //       systemProgram: SystemProgram.programId
      //     },
      //     signers: [],
      //   },
      // )
      
      oa_layout = await program.account.optionAccount.fetch(
        oa_pda
      )

      let matched_long_trader_long_mint_account = await tokenHandler(
        matched_long_trader_mint_pda,
        long_mint_pda
      )

      let matched_long_trader_short_mint_account = await tokenHandler(
        matched_long_trader_mint_pda,
        short_mint_pda
      )

      let matched_short_trader_long_mint_account = await tokenHandler(
        matched_long_trader_mint_pda,
        long_mint_pda
      )
      
      let matched_short_trader_short_mint_account = await tokenHandler(
        matched_long_trader_mint_pda,
        short_mint_pda
      )

      // console.log("orderbooks after match on chain")
      // // console.log("long_orderbook", long_orderbook)
      // // console.log("short_orderbook", short_orderbook)
    } else {
      console.log("no match found")
      console.log("long_orderbook", long_orderbook)
      console.log("short_orderbook", short_orderbook)
    }
    
    async function tokenHandler (
      userMintAccount: PublicKey,
      mint: PublicKey,
    ): Promise<any> {
      await provider.connection.getTokenAccountsByOwner(
        userMintAccount,
        {
          mint: mint,
          encoding: "jsonParsed"
        } as anchor.web3.TokenAccountsFilter
      ).then((res) => {
        let token_account = res.value[0].account as anchor.web3.AccountInfo<Buffer>
        let account_info = AccountLayout.decode(token_account.data)
        let account_mint = new PublicKey(account_info.mint)
        let account_owner = new PublicKey(account_info.owner)
        let account_amount_nu64 = nu64().decode(Buffer.from(account_info.amount, 'hex'))
        let account_closeAuthority = new PublicKey(account_info.closeAuthority)
        console.log("account_mint: ", account_mint.toBase58())
        console.log("account_owner: ", account_owner.toBase58())
        console.log("account_closeAuthority: ", account_closeAuthority.toBase58())
        console.log("account_amount_nu64: ", account_amount_nu64)
        // console.log("account: ", account_info)
        return account_info
      })
    }
  })
});


// const ERROR_DISCRIMINATOR = "Program log: AnchorError";
// const ERROR_MESSAGE_DISCRIMINATOR = "Error Message: ";

// export const parseAnchorError = (err: any) => {
//   const logs = err?.logs || [];

//   const errorLog = logs.find((l: string) => l.includes(ERROR_DISCRIMINATOR));

//   if (!errorLog) {
//     return null;
//   }

//   const message = errorLog.split(ERROR_MESSAGE_DISCRIMINATOR)[1];

//   return {
//     message,
//     log: errorLog,
//   };
// };

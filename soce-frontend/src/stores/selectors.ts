import { SoceStore } from "./useSoceStore" ;

export const actionsSelector = (state: SoceStore) => state.actions
export const pythAssetTypeSelectors = (state: SoceStore) => state.pythMarketProductDetails.assetTypes
export const pythProductFromSymbol = (state: SoceStore) => state.pythMarketProductDetails.productFromSymbol
export const pythProductPricingAll = (state: SoceStore) => state.pythMarketProductDetails.prices
export const pythSymbols = (state: SoceStore) => state.pythMarketProductDetails.symbols
export const pythProductPricing = (state: SoceStore) => state.pythMarketProductDetails.productPrice
export const pythAccountMapping = (state: SoceStore) => state.pythMarketProductDetails.productPriceAccountMapping

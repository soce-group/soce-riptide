import create, { State } from "zustand";
import produce from "immer";
import { Commitment, Connection, PublicKey, Transaction } from "@solana/web3.js";
import { EndpointInfo, OptionTokenAccounts, OrderInfo, OrderInfoDisplay, WalletAdapter, WalletToken } from "./../@types/types";
import { PythGetAllProductsAndPricesResult } from "./../pythhelpers/PythAllProductsAndPrices";
import { ConsolidatedProductPriceInfo } from "./../pythhelpers";
import { PythGetAllProductsAndPrices } from "./../pythhelpers/PythAllProductsAndPrices";
import { getPythProgramKeyForCluster } from "./../pythhelpers/cluster";
import { useConnection, Wallet } from "@solana/wallet-adapter-react";
import { Program, Provider } from "@project-serum/anchor";
import { Escrow } from "../constants/escrow";

export const ENDPOINTS: EndpointInfo[] = [
  {
    name: "mainnet",
    url: process.env.RPC_ENDPOINT || "https://api.devent.solana.com",
    websocket:
      process.env.RPC_WEBSOCKET_ENDPOINT || "https://api.devent.solana.com",
    custom: false,
  },
  {
    name: "devnet",
    url: "https://api.devnet.solana.com",
    websocket: "https://api.devnet.solana.com",
    custom: false,
  },
  {
    name: "localnet",
    url: "http://127.0.0.1:8899",
    websocket: "http://127.0.0.1:8899",
    custom: false,
  },
];

export interface Position {
  symbol: string;
  expiryDate: Date;
  strikePrice: number;
  timeTillExpirationInDays: number;
}

export interface Position {
  symbol: string;
  expiryDate: Date;
  strikePrice: number;
  timeTillExpirationInDays: number;
}
export interface PythPriceData {
  symbol: string;
  price: number;
  confidence: number;
}

export type LongPosition = Position;
export type ShortPosition = Position;
export type PositionDisplayData =
  | LongPosition[]
  | ShortPosition[]
  | PythPriceData[];

export interface SoceStore extends State {
  defaultOptions: any;
  pythMarketProductDetails: PythGetAllProductsAndPricesResult;
  selectedMarketSymbol: string;
  selectedMarketDetails: ConsolidatedProductPriceInfo;
  selectedBottomTab: string;
  showFloatingTab: boolean;
  floatingTabSelected: boolean;
  floatingElementData: PositionDisplayData;
  notifications: any[];
  notificationIdCounter: number;
  accountInfos: string[];
  anchor: {
    escrowProgramId: PublicKey;
    escrowProgram: Program<Escrow>;
    provider: Provider;
  };
  wallet: {
    current: Wallet;
    connected: boolean;
    connect: () => Promise<void>;
    disconnect: () => Promise<void>;
    signAllTransactions?: (
      transaction: Transaction[]
    ) => Promise<Transaction[]>;
    signTransaction?: (transaction: Transaction) => Promise<Transaction>;
    publicKey: PublicKey;
  };
  connection: {
    current: Connection;
    websocket: Connection;
  };
  tradeHistory: any[];
  userTradeHistory: OptionTokenAccounts[];
  userOpenOrders: OrderInfoDisplay[];
  set: (x: any) => void;
  actions: {
    fetchAllSoceAccounts: () => Promise<void>;
    fetchPythOraclePrices: () => any;
    [key: string]: (args?: any) => void;
  };
  selectedMarket: string;
  selectedOptionAccount: number;
}

const useSoceStore = create<SoceStore>((set, get) => {
  const rpcUrl = "http://127.0.0.1:8899";
  const connection = new Connection(rpcUrl, "processed" as Commitment);

  return {
    defaultOptions: null,
    selectedMarket: 'SOL/USD',
    selectedOptionAccount: 3,
    pythMarketProductDetails: {} as PythGetAllProductsAndPricesResult,
    selectedBottomTab: "",
    floatingTabSelected: false,
    floatingElementData: [] as PositionDisplayData,
    showFloatingTab: false,
    selectedMarketSymbol: "Nothing Selected Yet",
    selectedMarketDetails: {} as ConsolidatedProductPriceInfo,
    notificationIdCounter: 0,
    notifications: [],
    accountInfos: [],
    anchor: {
      escrowProgramId: null,
      escrowProgram: null,
      provider: null,
    },
    wallet: {
      current: null,
      connected: false,
      connect: () => {
        console.log("Wallet Not connected yet");
        return new Promise(() => "connection tbd");
      },
      disconnect: () => {
        console.log("Wallet Not connected yet");
        return new Promise(() => "connection tbd");
      },
      publicKey: null,
    },
    connection: {
      current: connection,
      websocket: connection,
    },
    tradeHistory: [],
    userTradeHistory: [],
    userOpenOrders: [],
    set: (fn: any) => set(produce(fn)),
    actions: {
      async fetchPythOraclePrices() {
        const setSoceStore = get().set;
        const connection = get().connection.current;
        try {
          const programKey = getPythProgramKeyForCluster("devnet");
          const pyth_client = new PythGetAllProductsAndPrices(
            connection,
            programKey
          );
          pyth_client.getData().then(
            (result) => {
              try {
                setSoceStore((state) => {
                  state.pythMarketProductDetails = result;
                });
              } catch (cerr) {
                console.log("there is an error");
                setSoceStore((state) => {
                  state.pythMarketProductDetails = {};
                });
              }
            },
            (err) => console.log(err)
          );
        } catch (err_catch) {
          console.log(err_catch);
        }
      },
      async fetchWalletTokens() {
        console.log("get all the Wallet tokens");
      },
      async fetchAllSoceAccounts() {
        console.log("get all the account info");
        // const connection = get().connection.current;
        // await tokenHandler(
        //   connection,
        //   userMintAccount,
        //   state.defaultOptions.allTransactionInformation[0]
        // );
      },
      async fetchTradeHistory(soceAccount = null) {
        console.log("get all the trade history");
      },
    },
  };
});

export default useSoceStore;

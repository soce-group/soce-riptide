import "./highcharts.css";
import axios from "axios";
import React from "react";
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import dayjs from "dayjs";

// configure all the indicators
import indicatorsAll from "highcharts/indicators/indicators-all";
import annotationsAdvanced from "highcharts/modules/annotations-advanced";
import priceIndicator from "highcharts/modules/price-indicator";
import fullScreen from "highcharts/modules/full-screen";
import stockTools from "highcharts/modules/stock-tools";

// import { pythResponse } from "./responseFromPyth";

const soceApiEndPoint = "http://soce.io/api";
const soceAxiosApi = axios.create({
  baseURL: soceApiEndPoint,
  timeout: 1000,
});

export const getCurrentPrices = async function (
  market: string,
  startTime: number,
  endTime: number
): Promise<any> {
  const params = {
    symbol: market,
    resolution: 1,
    from: startTime,
    to: endTime,
  };

  const res = await soceAxiosApi.get(`/pricehistory/tv/history`, { params });
  const data = await res.data;
  return data;
};

indicatorsAll(Highcharts);
annotationsAdvanced(Highcharts);
priceIndicator(Highcharts);
fullScreen(Highcharts);
stockTools(Highcharts);

function range(start: number, end: number) {
  return Array(end - start)
    .fill(0)
    .map((_, idx) => start + idx);
}

const converPythToHighchartsFormat = function (pythFormat: any) {
  const { t, o, h, l, c } = pythFormat;
  const totalPriceItems = t.length;
  const rangeArray = range(0, totalPriceItems);
  const transformedData: any[] = [];
  rangeArray.forEach((idx: number) => {
    const dataArray: any = [+t[idx] * 1000, +o[idx], +h[idx], +l[idx], +c[idx]];
    transformedData.push(dataArray);
  });
  return transformedData;
};

const groupingUnits = [
  ["minute", [15, 30]],
  ["hour", [1, 2, 3, 4, 6, 8, 12]],
  ["hr", [1]],
  ["day", [1]],
  ["week", [1]],
  ["month", [1, 2, 3, 4, 6]],
];

const createChartOptions = function (data: any) {
  const options = {
    rangeSelector: {
      selected: 1,
    },
    title: {
      text: "Chart - SOL/USD",
    },

    yAxis: [
      {
        labels: {
          align: "left",
          x: -3,
        },
        title: {
          text: "OHLC",
        },
        height: "100%",
        lineWidth: 2,
        resize: {
          enabled: true,
        },
      },
    ],
    tooltip: {
      split: false,
    },

    series: [
      {
        type: "candlestick",
        name: "SOL/USD",
        data,
        dataGrouping: {
          units: groupingUnits,
        },
      },
    ],
  };
  return options;
};

const SoceCharts = function () {
  const [currentMarket, setMarket] = React.useState<string>("SOL/USD");
  const [options, setOptions] = React.useState<any>(createChartOptions([]));

  React.useEffect(() => {
    const interval = setInterval(() => {
      const currentTime = dayjs();
      const currentUnixDate = currentTime.unix();
      const historicalUnixDate = currentTime.subtract(5, "day").unix();
      // const transformedData = converPythToHighchartsFormat(pythResponse);
      // const options = createChartOptions(transformedData);
      setOptions(options);
      getCurrentPrices(currentMarket, historicalUnixDate, currentUnixDate).then(
        (data) => {
          const transformedData = converPythToHighchartsFormat(data);
          // const transformedData = converPythToHighchartsFormat(pythResponse);
          const options = createChartOptions(transformedData);
          setOptions(options);
        }
      );
    }, 5000);
    return () => clearInterval(interval);
  }, [currentMarket]);

  return (
    <HighchartsReact
      containerProps={{ style: { height: "100%" } }}
      highcharts={Highcharts}
      constructorType={"stockChart"}
      options={options}
    />
  );
};

export default SoceCharts;

import { useEffect, useRef, useState } from "react";
import {
  widget,
  ChartingLibraryWidgetOptions,
  IChartingLibraryWidget,
  ResolutionString,
} from "./../../../public/charting_library";
import { CHART_DATA_FEED } from "../../utils/chartDataConnector";
import useSoceStore from "../../stores/useSoceStore";
import { useViewport } from "../../hooks/useViewport";
import { formatUsdValue, usdFormatter } from "../../utils";
import useInterval from "../../hooks/useInterval";
import { useTranslation } from "react-i18next";
import { useWallet } from "@solana/wallet-adapter-react";

// This is a basic example of how to create a TV widget
// You can add more feature such as storing charts in localStorage

export interface ChartContainerProps {
  symbol: ChartingLibraryWidgetOptions["symbol"];
  interval: ChartingLibraryWidgetOptions["interval"];
  datafeedUrl: string;
  libraryPath: ChartingLibraryWidgetOptions["library_path"];
  chartsStorageUrl: ChartingLibraryWidgetOptions["charts_storage_url"];
  chartsStorageApiVersion: ChartingLibraryWidgetOptions["charts_storage_api_version"];
  clientId: ChartingLibraryWidgetOptions["client_id"];
  userId: ChartingLibraryWidgetOptions["user_id"];
  fullscreen: ChartingLibraryWidgetOptions["fullscreen"];
  autosize: ChartingLibraryWidgetOptions["autosize"];
  studiesOverrides: ChartingLibraryWidgetOptions["studies_overrides"];
  containerId: ChartingLibraryWidgetOptions["container_id"];
  theme: string;
}

// export interface ChartContainerState {}

const TVChartContainer = () => {
  const { t } = useTranslation(["common", "tv-chart"]);
  const { connected , wallet } = useWallet();
  const [lines, setLines] = useState(new Map());
  const [moveInProgress, toggleMoveInProgress] = useState(false);
  const [orderInProgress, toggleOrderInProgress] = useState(false);
  const [priceReset, togglePriceReset] = useState(false);
  const [showOrderLines, toggleShowOrderLines] = useState(true);

  // @ts-ignore
  const defaultProps: ChartContainerProps = {
    symbol: "SOL/USD",
    interval: "60" as ResolutionString,
    theme: "Dark",
    containerId: "tv_chart_container",
    datafeedUrl: CHART_DATA_FEED,
    libraryPath: "/charting_library/",
    fullscreen: false,
    autosize: true,
    studiesOverrides: {
      "volume.volume.color.0": "#E54033",
      "volume.volume.color.1": "#AFD803",
      "volume.precision": 4,
    },
  };

  const tvWidgetRef = useRef<IChartingLibraryWidget | null>(null);

  useEffect(() => {
    if (
      tvWidgetRef.current &&
      // @ts-ignore
      tvWidgetRef.current._innerAPI()
    ) {
      tvWidgetRef.current.setSymbol("SOL/USD", defaultProps.interval, () => {});
    }
  }, ["SOL/USD"]);

  useEffect(() => {
    const widgetOptions: ChartingLibraryWidgetOptions = {
      symbol: "SOL/USD",
      // BEWARE: no trailing slash is expected in feed URL
      // tslint:disable-next-line:no-any
      datafeed: new (window as any).Datafeeds.UDFCompatibleDatafeed(
        defaultProps.datafeedUrl
      ),
      interval:
        defaultProps.interval as ChartingLibraryWidgetOptions["interval"],
      container_id:
        defaultProps.containerId as ChartingLibraryWidgetOptions["container_id"],
      library_path: defaultProps.libraryPath as string,
      locale: "en",
      enabled_features: ["hide_left_toolbar_by_default"],
      disabled_features: [
        "use_localstorage_for_settings",
        "timeframes_toolbar",
        // 'volume_force_overlay',
        "show_logo_on_all_charts",
        "caption_buttons_text_if_possible",
        "header_settings",
        "header_chart_type",
        "header_compare",
        "compare_symbol",
        "header_screenshot",
        // 'header_widget_dom_node',
        // 'header_widget',
        "header_saveload",
        "header_undo_redo",
        "header_interval_dialog_button",
        "show_interval_dialog_on_key_press",
        "header_symbol_search",
      ],
      load_last_chart: true,
      client_id: defaultProps.clientId,
      user_id: defaultProps.userId,
      fullscreen: defaultProps.fullscreen,
      autosize: defaultProps.autosize,
      studies_overrides: defaultProps.studiesOverrides,
      custom_css_url: "/tradingview-chart.css",
      loading_screen: {
        backgroundColor: "#fff",
      },
      overrides: {
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
        "paneProperties.background": "#fff",
        "mainSeriesProperties.candleStyle.barColorsOnPrevClose": true,
        "mainSeriesProperties.candleStyle.drawWick": true,
        "mainSeriesProperties.candleStyle.drawBorder": true,
        "mainSeriesProperties.candleStyle.upColor": "#AFD803",
        "mainSeriesProperties.candleStyle.downColor": "#E54033",
        "mainSeriesProperties.candleStyle.borderColor": "#AFD803",
        "mainSeriesProperties.candleStyle.borderUpColor": "#AFD803",
        "mainSeriesProperties.candleStyle.borderDownColor": "#E54033",
        "mainSeriesProperties.candleStyle.wickUpColor": "#AFD803",
        "mainSeriesProperties.candleStyle.wickDownColor": "#E54033",
      },
    };

    const tvWidget = new widget(widgetOptions);
    tvWidgetRef.current = tvWidget;
    tvWidgetRef.current.onChartReady(function () {
      const button = tvWidgetRef.current.createButton();
      button.textContent = "OL";
      button.style.color = "rgb(255, 156, 36)";
      button.setAttribute("title", t("tv-chart:toggle-order-line"));
      button.addEventListener("click", function () {
        toggleShowOrderLines((showOrderLines) => !showOrderLines);
        if (
          button.style.color === "rgb(255, 156, 36)" ||
          button.style.color === "rgb(242, 201, 76)"
        ) {
          button.style.color = "rgb(138, 138, 138)";
        } else {
          button.style.color = "rgb(255, 156, 36)";
        }
      });
    });
    //eslint-disable-next-line
  }, []);

  const handleCancelOrder = async (
    order: any,
    market: any
  ) => {
  };

  const handleModifyOrder = async (
    order: any,
    market: any,
    price: number
  ) => {
    console.log("Need to write Custom Logic for this");
  };

  function getLine(order, market) {
    return tvWidgetRef.current
      .chart()
      .createOrderLine({ disableUndo: false })
      .onMove(function () {
        toggleMoveInProgress(true);
        toggleOrderInProgress(true);
        const currentOrderPrice = order.price;
        const updatedOrderPrice = this.getPrice();
        if (!order.perpTrigger?.clientOrderId) {
          if (
            (order.side === "buy" && updatedOrderPrice > 1.05) ||
            (order.side === "sell" && updatedOrderPrice < 0.95)
          ) {
            tvWidgetRef.current.showNoticeDialog({
              title: t("tv-chart:outside-range"),
              body:
                t("tv-chart:slippage-warning", {
                  updatedOrderPrice: formatUsdValue(updatedOrderPrice),
                  aboveBelow: order.side == "buy" ? t("above") : t("below"),
                  selectedMarketPrice: formatUsdValue(5.4567),
                }) +
                "<p><p>" +
                t("tv-chart:slippage-accept"),
              callback: () => {
                this.setPrice(currentOrderPrice);
                toggleMoveInProgress(false);
                toggleOrderInProgress(false);
              },
            });
          } else {
            tvWidgetRef.current.showConfirmDialog({
              title: t("tv-chart:modify-order"),
              body: t("tv-chart:modify-order-details", {
                orderSize: order.size,
                baseSymbol: market.config.baseSymbol,
                orderSide: t(order.side),
                currentOrderPrice: currentOrderPrice,
                updatedOrderPrice: updatedOrderPrice,
              }),
              callback: (res) => {
                if (res) {
                  handleModifyOrder(order, market.account, updatedOrderPrice);
                } else {
                  this.setPrice(currentOrderPrice);
                  toggleOrderInProgress(false);
                  toggleMoveInProgress(false);
                }
              },
            });
          }
        } else {
          tvWidgetRef.current.showNoticeDialog({
            title: t("tv-chart:advanced-order"),
            body: t("tv-chart:advanced-order-details"),
            callback: () => {
              this.setPrice(currentOrderPrice);
              toggleMoveInProgress(false);
              toggleOrderInProgress(false);
            },
          });
        }
      })
      .onCancel(function () {
        toggleOrderInProgress(true);
        tvWidgetRef.current.showConfirmDialog({
          title: t("tv-chart:cancel-order"),
          body: t("tv-chart:cancel-order-details", {
            orderSize: order.size,
            baseSymbol: market.config.baseSymbol,
            orderSide: t(order.side),
            orderPrice: order.price,
          }),
          callback: (res) => {
            if (res) {
              handleCancelOrder(order, market.account);
            } else {
              toggleOrderInProgress(false);
            }
          },
        });
      })
      .setPrice(order.price)
      .setQuantity(order.size)
      .setText(getLineText(order, market))
      .setTooltip(
        order.perpTrigger?.clientOrderId
          ? `${order.orderType} Order #: ${order.orderId}`
          : `Order #: ${order.orderId}`
      )
      .setBodyTextColor("#FF9C24")
      .setQuantityTextColor("#F2C94C")
      .setCancelButtonIconColor("#F2C94C")
      .setBodyBorderColor("#FF9C24")
      .setQuantityBorderColor("#FF9C24")
      .setCancelButtonBorderColor("#FF9C24")
      .setBodyBackgroundColor("#1D1832")
      .setQuantityBackgroundColor("#1D1832")
      .setCancelButtonBackgroundColor("#1D1832")
      .setBodyFont("Lato, sans-serif")
      .setQuantityFont("Lato, sans-serif")
      .setLineColor(
        order.perpTrigger?.clientOrderId
          ? "#FF9C24"
          : order.side == "buy"
          ? "#4BA53B"
          : "#AA2222"
      )
      .setLineLength(3)
      .setLineWidth(2)
      .setLineStyle(1);
  }

  function getLineText(order, market) {
    const orderSideTranslated = t(order.side);
    if (order.perpTrigger?.clientOrderId) {
      const triggerPrice =
        order.perpTrigger.triggerPrice *
        Math.pow(10, market.config.baseDecimals - market.config.quoteDecimals);
      const orderTypeTranslated = t(order.orderType);
      const triggerConditionTranslated = t(order.perpTrigger.triggerCondition);
      if (order.side === "buy") {
        if (order.perpTrigger.triggerCondition === "above") {
          return (
            (order.orderType === "market" ? t("stop-loss") : t("stop-limit")) +
            t("tv-chart:order-details", {
              orderType: orderTypeTranslated,
              orderSide: orderSideTranslated,
              triggerCondition: triggerConditionTranslated,
              triggerPrice: usdFormatter(triggerPrice),
            })
          );
        } else {
          return (
            t("take-profit") +
            t("tv-chart:order-details", {
              orderType: orderTypeTranslated,
              orderSide: orderSideTranslated,
              triggerCondition: triggerConditionTranslated,
              triggerPrice: usdFormatter(triggerPrice),
            })
          );
        }
      } else {
        if (order.perpTrigger.triggerCondition === "below") {
          return (
            (order.orderType === "market" ? t("stop-loss") : t("stop-limit")) +
            t("tv-chart:order-details", {
              orderType: orderTypeTranslated,
              orderSide: orderSideTranslated,
              triggerCondition: triggerConditionTranslated,
              triggerPrice: usdFormatter(triggerPrice),
            })
          );
        } else {
          return (
            t("take-profit") +
            t("tv-chart:order-details", {
              orderType: orderTypeTranslated,
              orderSide: orderSideTranslated,
              triggerCondition: triggerConditionTranslated,
              triggerPrice: usdFormatter(triggerPrice),
            })
          );
        }
      }
    } else {
      return `${orderSideTranslated} ${market.config.baseSymbol}`.toUpperCase();
    }
  }

  function deleteLines() {
    tvWidgetRef.current.onChartReady(() => {
      if (lines?.size > 0) {
        lines?.forEach((value, key) => {
          lines.get(key).remove();
        });
      }
    });
    return new Map();
  }

  function drawLines() {
    const tempLines = new Map();
    tvWidgetRef.current.onChartReady(() => {});
    return tempLines;
  }

  useInterval(() => {
    if (showOrderLines) {
      if (connected) {
        let matches = 0;
        let openOrdersInSelectedMarket = 0;
        if (
          lines?.size != openOrdersInSelectedMarket ||
          matches != openOrdersInSelectedMarket ||
          (lines?.size > 0 && lines?.size != matches) ||
          priceReset
        ) {
          if (priceReset) {
            togglePriceReset(false);
          }
          setLines(deleteLines());
          setLines(drawLines());
        }
      } else if (lines?.size > 0 && !moveInProgress && !orderInProgress) {
        setLines(deleteLines());
      }
    } else if (lines?.size > 0) {
      setLines(deleteLines());
    }
  }, [500]);

  return <div id={defaultProps.containerId} className="tradingview-chart" />;
};

export default TVChartContainer;

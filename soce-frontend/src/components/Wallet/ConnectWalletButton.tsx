import { Fragment, useState } from "react";
import useSoceStore from "../../stores/useSoceStore";
import {
  LogoutIcon,
} from "@heroicons/react/outline";
import { PROVIDER_LOCAL_STORAGE_KEY } from "../../hooks/useWallet";
import useLocalStorageState from "../../hooks/useLocalStorageState";
import { abbreviateAddress} from "../../utils";
import WalletSelect from "./WalletSelect";
import { WalletIcon } from "../CommonComponents/Icons";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  DEFAULT_PROVIDER,
  WALLET_PROVIDERS,
} from "../../utils/wallet-adapters";
import { useWallet } from "@solana/wallet-adapter-react";

const ConnectWalletButton = () => {
  const { t } = useTranslation("common");
  const { wallet, connected, connect , publicKey: walletPublicKey, disconnect} = useWallet();
  const [selectedWallet, setSelectedWallet] = useState(DEFAULT_PROVIDER.url);
  const [savedProviderUrl] = useLocalStorageState(
    PROVIDER_LOCAL_STORAGE_KEY,
    DEFAULT_PROVIDER.url
  );
  // update in useEffect to prevent SRR error from next.js
  useEffect(() => {
    setSelectedWallet(savedProviderUrl);
  }, [savedProviderUrl]);

  const handleWalletConect = () => {
    connect();
  };

  return (
    <>
      {connected && walletPublicKey? (
        <button
          className="flex w-full flex-row justify-end items-center rounded-none pr-3 py-0.5 font-normal hover:cursor-pointer hover:text-th-primary focus:outline-none"
          onClick={() => disconnect()}
        >
          <LogoutIcon className="h-4 w-4" />
          <div className="pl-2 text-left">
            <div className="pb-0.5">{t("disconnect")}</div>
            <div className="text-xs text-th-fgd-4">
              {abbreviateAddress(walletPublicKey)}
            </div>
          </div>
        </button>
      ) : (
        <div
          className="flex z-10 h-14 justify-end divide-x divide-th-bkg-3"
          id="connect-wallet-tip"
        >
          <button
            onClick={handleWalletConect}
            disabled={!wallet}
            className="rounded-none bg-th-primary-dark text-th-bkg-1 hover:brightness-[1.1] focus:outline-none disabled:cursor-wait disabled:text-th-bkg-2"
          >
            <div className="default-transition flex h-full flex-row items-center justify-center px-3">
              <WalletIcon className="mr-2 h-4 w-4 fill-current" />
              <div className="text-left">
                <div className="mb-0.5 whitespace-nowrap font-bold">
                  {t("Connect")}
                </div>
                <div className="text-xxs font-normal leading-3 tracking-wider text-th-bkg-2">
                  {WALLET_PROVIDERS.find((p) => p.url === selectedWallet)?.name}
                </div>
              </div>
            </div>
          </button>
          <div className="relative z-50">
            <WalletSelect />
          </div>
        </div>
      )}
    </>
  );
};

export default ConnectWalletButton;

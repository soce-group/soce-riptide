import React from "react";
import styled from "styled-components";

const SoceDiv = styled.div`
  display: grid;
  height: 100vh;
  weidth: 100vw;
  grid-template-columns: auto;
  grid-template-rows: 3rem 3rem 3rem 1fr 3rem;
  grid-template-areas:
    "hd"
    "sel"
    "search"
    "main"
    "ft";
`;

const Header = styled.div`
  grid-area: hd;
`;

const MarketSelections = styled.div`
  grid-area: sel;
  background-color: yellow;
`;
const MarketSearchBar = styled.div`
  grid-area: search;
  background-color: pink;
`;

const MainContent = styled.div`
  grid-area: main;
`;

const Positions = styled.div`
  grid-area: ft;
  background-color: beige;
`;

export default function LandingPageWithStyledComponents() {
  return (
    <SoceDiv>
      <Header />
      <MarketSelections />
      <MarketSearchBar />
      <MainContent />
      <Positions />
    </SoceDiv>
  );
}

import { Link } from 'react-router-dom'
import { ChevronRightIcon } from '@heroicons/react/solid'

const MenuItem = ({ href, children, newWindow = false }) => {
  return (
    <Link to={href}>
      <a
        className={`flex h-full items-center justify-between border-b border-th-bkg-4 p-3 font-bold text-th-fgd-1 hover:text-th-primary md:border-none md:py-0 text-th-primary`}
        target={newWindow ? '_blank' : ''}
        rel={newWindow ? 'noopener noreferrer' : ''}
      >
        {children}
        <ChevronRightIcon className="h-5 w-5 md:hidden" />
      </a>
    </Link>
  )
}

export default MenuItem

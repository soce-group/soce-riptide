import React, { useState } from "react";
import BottomFloatingPositions from "./BottomFloatingPositions";
import TradingForm from "./TradingForm";
import SoceGraph from "./Graph";
import { useWallet } from "@solana/wallet-adapter-react";
import { WalletMultiButton } from "@solana/wallet-adapter-react-ui";

export default function MainSoceContent() {
  const [showFloatingPanel] = useState<boolean>(true);
  const { connected: walletEnabled } = useWallet();

  return (
    <div className="flex h-full flex-row">
      <div className="flex-1 bg-gray-100">
        <SoceGraph />
      </div>
      <TradingSection walletEnabled={walletEnabled} />
      {showFloatingPanel && <BottomFloatingPositions />}
    </div>
  );
}

export function TradingSection({ walletEnabled }: { walletEnabled: boolean }) {
  return (
    <div className="p-2 w-1/4 bg-gray-400 flex flex-col items-center justify-evenly relative z-10">
      {walletEnabled && <TradingForm />}
      {!walletEnabled && <WalletNotEnabledOverlay />}
    </div>
  );
}

export function WalletNotEnabledOverlay() {
  return (
    <div className="absolute flex flex-col items-center justify-center top-0 bottom-0 left-0 right-0 opacity-80 bg-gray-700">
      <div className="text-white bg-indigo-700 text-lg border-4 border-indigo-100 p-2 rounded cursor-pointer hover:scale-125 transition-all delay-300 ">
        <WalletMultiButton />
      </div>
    </div>
  );
}

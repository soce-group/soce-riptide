import { Fragment, useCallback, useState } from "react";
import { Transition, Combobox } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import useSoceStore, { SoceStore } from "./../../stores/useSoceStore";

export default function MarketSelection() {
  const [selected, setSelected] = useState("");
  const [query, setQuery] = useState("");
  const marketSymbols = useSoceStore(
    (state) => state.pythMarketProductDetails.symbols
  );
  const pythProductDetails = useSoceStore(
    (state) => state.pythMarketProductDetails
  );
  const setSoceState = useSoceStore((state) => state.set);

  const filteredMarketGetter = useCallback(() => {
    return query === ""
      ? marketSymbols
      : marketSymbols.filter((market) =>
          market
            .toLowerCase()
            .replace(/\s+/g, "")
            .includes(query.toLowerCase().replace(/\s+/g, ""))
        );
  }, [marketSymbols, query]);

  function onChangeSelect(marketSelected: string) {
    console.log("is the on change running as well");
    setSelected(marketSelected);
    const { consolidatedProductPriceDetails } = pythProductDetails;
    const selectedMarketInfo =
      consolidatedProductPriceDetails.get(marketSelected);
    setSoceState((state: SoceStore) => {
      state.selectedMarketDetails = selectedMarketInfo;
      state.selectedMarketSymbol = marketSelected;
    });
  }

  return (
    <div className="w-[700px] place-self-center z-10">
      <Combobox value={selected} onChange={onChangeSelect} disabled>
        <div className="relative mt-1">
          <div className="relative w-full text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-gray-300 focus-visible:ring-offset-2 sm:text-sm overflow-hidden">
            <Combobox.Input
              className="w-full border-none focus:ring-0 py-2 pl-3 pr-10 text-sm leading-5 text-gray-900"
              displayValue={(m) => "Pyth Selections Comming Soon"}
              onChange={(event) => setQuery(event.target.value)}
            />
            <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
              <SelectorIcon
                className="w-5 h-5 text-gray-400"
                aria-hidden="true"
              />
            </Combobox.Button>
          </div>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
            afterLeave={() => setQuery("")}
          >
            <Combobox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {(filteredMarketGetter() || []).length === 0 && query !== "" ? (
                <div className="cursor-default select-none relative py-2 px-4 text-gray-700">
                  Nothing found.
                </div>
              ) : (
                (filteredMarketGetter() || []).map((market) => (
                  <Combobox.Option
                    key={market}
                    className={({ active }) =>
                      `cursor-default select-none relative py-2 pl-10 pr-4 ${
                        active ? "text-white bg-gray-600" : "text-gray-900"
                      }`
                    }
                    onClick={(e) => console.log("selected", e)}
                    value={market}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={`block truncate ${
                            selected ? "font-medium" : "font-normal"
                          }`}
                        >
                          {market}
                        </span>
                        {selected ? (
                          <span
                            className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                              active ? "text-white" : "text-gray-600"
                            }`}
                          >
                            <CheckIcon className="w-5 h-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </Combobox.Option>
                ))
              )}
            </Combobox.Options>
          </Transition>
        </div>
      </Combobox>
    </div>
  );
}

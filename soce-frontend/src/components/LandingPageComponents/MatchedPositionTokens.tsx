import { token } from "@project-serum/anchor/dist/cjs/utils";
import { PublicKey } from "@solana/web3.js";
import React, { ReactElement, ReactNode, useEffect, useMemo } from "react";
import { OptionTokenAccounts, ParsedToken } from "../../@types/types";
import useSoceStore, { SoceStore } from "../../stores/useSoceStore";
import PythMarketsById from "../../utils/pythMarketIds";
import { tokenHandler } from "../../utils/tokens";



export default function MatchedPositionTokens() {

  const connection = useSoceStore(
    (state) => state.connection.current
  )

  const wallet = useSoceStore(
    (state) => state.wallet
  )

  const escrowProgram = useSoceStore(
    (state) => state.anchor.escrowProgram
  )

  const defaultOptions = useSoceStore(
    (state) => state.defaultOptions.allTransactionInformation
  )
  
  const userMintSeeds = [
    wallet.publicKey.toBuffer()
  ]

  const userTradeHistory = useSoceStore(
    (state) => state.userTradeHistory
  )

  const setSoceStore = useSoceStore((state: SoceStore) => state.set);

  async function tokenAccountsGetter () {
    let promises = defaultOptions.map(async (option) => {

      let [ userPda, bump ] = await PublicKey.findProgramAddress(
        userMintSeeds,
        escrowProgram.programId
      );

      let longMintTokenAccount: ParsedToken = await tokenHandler(
        connection,
        userPda,
        option.pdaAccounts.longMintPda
      )

      let shortMintTokenAccount: ParsedToken = await tokenHandler(
        connection,
        userPda,
        option.pdaAccounts.shortMintPda
      )

      let optionTokenAccount: OptionTokenAccounts = {
        oracleAssetKey: option.oracleAssetKey,
        expiry: option.optionTradeDetailsRaw.expiry,
        strike: option.optionTradeDetailsRaw.strike,
        strikeExponent: option.optionTradeDetailsRaw.strikeExponent,
        userLongTokenAccount: longMintTokenAccount,
        userShortTokenAccount: shortMintTokenAccount
      }

      return optionTokenAccount

    })

    const _userTradeHistory: OptionTokenAccounts[] = await Promise.all(promises)


    setSoceStore((state: SoceStore) => {
      state.userTradeHistory = _userTradeHistory;
    });

  }

  useEffect(() => {
   
    tokenAccountsGetter()

    return () => {}
  }, [])

  function tokenDisplay(account: OptionTokenAccounts): ReactElement {

    let tokenAccount
    if (
      !account.userLongTokenAccount
      && !account.userShortTokenAccount
    ) {
      return
    }
    let market = PythMarketsById[account.oracleAssetKey.toBase58()]

    if (account.userLongTokenAccount && account.userShortTokenAccount) {
      tokenAccount = (
        <div>
          <li key={"a"}>Market: {market}</li>
          <li key={"b"}>Expiry: {new Date(account.expiry * 1000).toDateString()}</li>
          <li key={"c"}>Strike: {account.strike * Math.pow(10, account.strikeExponent)}</li>
          <li key={"d"}>Quantity Short: {account.userLongTokenAccount.amount}</li>
          <li key={"e"}>Quantity Short: {account.userShortTokenAccount.amount}</li>
        </div>
      )
    }
    if (!account.userLongTokenAccount) {
      tokenAccount = (
        <div>
          <li key={"a"}>Market: {market}</li>
          <li key={"b"}>Expiry: {new Date(account.expiry * 1000).toDateString()}</li>
          <li key={"c"}>Strike: {account.strike * Math.pow(10, account.strikeExponent)}</li>
          <li key={"d"}>Quantity Short: {account.userShortTokenAccount.amount}</li>
        </div>
      )
    }
    if (!account.userShortTokenAccount) {
      tokenAccount = (
        <div>
          <li key={"a"}>Market: {market}</li>
          <li key={"b"}>Expiry: {new Date(account.expiry * 1000).toDateString()}</li>
          <li key={"c"}>Strike: {account.strike * Math.pow(10, account.strikeExponent)}</li>
          <li key={"d"}>Quantity Short: {account.userLongTokenAccount.amount}</li>
        </div>
      )
    }

    return (
      <div className="text-white bg-gradient-to-r from-gray-500 via-gray-600 to-gray-700 hover:bg-gradient-to-br focus:ring-1 focus:ring-gray-300 dark:focus:ring-gray-500 shadow-lg shadow-gray-500/50 dark:shadow-lg dark:shadow-gray-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-left mr-2">
        {tokenAccount}
      </div>
    )
  }


  return (
    <div>
      {userTradeHistory.map((account) =>
        <div key={JSON.stringify(account)}>
          {tokenDisplay(account)}
        </div>
      )}
    </div>
  );
}

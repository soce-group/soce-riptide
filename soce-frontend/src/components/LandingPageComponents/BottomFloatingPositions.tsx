import React from "react";
import { XCircleIcon } from "@heroicons/react/outline";

import useSoceStore, { SoceStore } from "../../stores/useSoceStore";

const columnHeaderMapping = {
  prices: {
    columns: ["Symbol", "Price", "Confidence"],
    displayTitle: "Prices Of All Assets From Pyth - Oracle",
  },
};

export default function FloatingMarketPositions() {
  const dataForDisplay = useSoceStore(
    (state: SoceStore) => state.floatingElementData
  );
  const tabSelected = useSoceStore(
    (state: SoceStore) => state.selectedBottomTab
  );
  return (
    <DisplayTableData tableData={dataForDisplay} tableTitle={tabSelected} />
  );
}

function DisplayTableData({
  tableTitle,
  tableData,
}: {
  tableTitle: string;
  tableData: any[];
}) {
  const setSoceState = useSoceStore((state: SoceStore) => state.set);
  const showFloatingTab = useSoceStore(
    (state: SoceStore) => state.showFloatingTab
  );
  const displayTable = tableData.length > 0 && showFloatingTab;
  return (
    displayTable && (
      <div className="flex flex-col absolute left-0 right-0 bottom-0 top-1/3 bg-blue-200 opacity-90 overflow-auto z-20">
        <div className="px-10 py-3 flex flex-col items-stretch justify-items-stretch relative inset-0 bg-blue-200 opacity-90 overflow-auto">
          <button
            onClick={() =>
              setSoceState((state: SoceStore) => {
                state.showFloatingTab = false;
              })
            }
            className="absolute w-16 h-16 text-indigo-900 right-3 cursor-pointer"
          >
            <span>
              <XCircleIcon />
            </span>
          </button>
          <div className="place-self-stretch font-bold bg-blue-500">
            <h1 className="text-center">
              {columnHeaderMapping[tableTitle].displayTitle}
            </h1>
          </div>
          <table className="table-fixed border border-collapse border-indigo-700">
            <thead>
              <tr>
                {columnHeaderMapping[tableTitle].columns.map((c) => (
                  <th key={c}>{c}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {(tableData || []).map(({ symbol, price, confidence }) => (
                <tr
                  className="text-center border border-indigo-500 border-collapse"
                  key={symbol}
                >
                  <td>{symbol}</td>
                  <td>{price}</td>
                  <td>{confidence}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    )
  );
}

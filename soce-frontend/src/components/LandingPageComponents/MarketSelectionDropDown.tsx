import useLocalStorageState from "../../hooks/useLocalStorageState";
import { StarIcon } from "@heroicons/react/solid";
import { QuestionMarkCircleIcon } from "@heroicons/react/outline";
import useSoceStore from "../../stores/useSoceStore";
import { useViewport } from "../../hooks/useViewport";
import * as MonoIcons from "./../CommonComponents/Icons";
import { Transition } from "@headlessui/react";
import { Link } from "react-router-dom";

const FavoritesShortcutBar = () => {
  const [favoriteMarkets] = useLocalStorageState("favoriteMarkets", []);
  const marketInfo = useSoceStore((s) => s.pythMarketProductDetails);
  const { width } = useViewport();

  const renderIcon = (symbol) => {
    const iconName = `${symbol.slice(0, 1)}${symbol
      .slice(1, 4)
      .toLowerCase()}MonoIcon`;

    const SymbolIcon = MonoIcons[iconName] || QuestionMarkCircleIcon;
    return <SymbolIcon className={`mr-1.5 h-3.5 w-auto`} />;
  };

  return (
    <Transition
      appear={true}
      className="flex items-center space-x-4 bg-th-bkg-3 px-4 py-2 xl:px-6"
      show={favoriteMarkets.length > 0}
      enter="transition-all ease-in duration-200"
      enterFrom="opacity-0"
      enterTo="opacity-100"
      leave="transition ease-out duration-200"
      leaveFrom="opacity-100"
      leaveTo="opacity-0"
    >
      <StarIcon className="h-5 w-5 text-th-fgd-4" />
      {/* {favoriteMarkets.map((mkt) => {
        const mktInfo = marketInfo.find((info) => info.name === mkt.name);
        return (
          <Link to={`/?name=${mkt.name}`} key={mkt.name}>
            <a
              className={`default-transition flex items-center whitespace-nowrap py-1 text-xs hover:text-th-primary`}
            >
              {renderIcon(mkt.baseSymbol)}
              <span className="mb-0 mr-1.5 text-xs">{mkt.name}</span>
              <span
                className={`text-xs ${
                  mktInfo
                    ? mktInfo.change24h >= 0
                      ? "text-th-green"
                      : "text-th-red"
                    : "text-th-fgd-4"
                }`}
              >
                {mktInfo ? `${(mktInfo.change24h * 100).toFixed(1)}%` : ""}
              </span>
            </a>
          </Link>
        );
      })} */}
    </Transition>
  );
};

export default FavoritesShortcutBar;

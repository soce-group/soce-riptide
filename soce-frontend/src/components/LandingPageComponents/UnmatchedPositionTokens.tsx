import { PublicKey } from "@solana/web3.js";
import React, { ReactElement, ReactNode, useEffect, useMemo } from "react";
import { OrderInfo, OrderInfoDisplay } from "../../@types/types";
import CancelTrade from "../../instructions/cancelTrade";
import useSoceStore, { SoceStore } from "../../stores/useSoceStore";
import PythMarketsById from "../../utils/pythMarketIds";



export default function UnmatchedPositionTokens() {

  const connection = useSoceStore(
    (state) => state.connection.current
  )

  const wallet = useSoceStore(
    (state) => state.wallet
  )
  const escrowProgram = useSoceStore(
    (state) => state.anchor.escrowProgram
  )

  const defaultOptions = useSoceStore(
    (state) => state.defaultOptions.allTransactionInformation
  )
  const userOpenOrders = useSoceStore(
    (state) => state.userOpenOrders
  )

  const setSoceStore = useSoceStore((state: SoceStore) => state.set);

  const anchorProvider = useSoceStore((state) => state.anchor.provider);

  const selectedOptionAccount = useSoceStore(
    (state) => state.selectedOptionAccount
  )

  const selectedOption = useSoceStore(
    (state) => state.defaultOptions.allTransactionInformation[selectedOptionAccount]
  )
  


  async function getOptionAccount(pda) {
    let optionAccountLayout = await escrowProgram.account.optionAccount.fetch(
      pda
    )
    return optionAccountLayout
  }
    



  async function searchOrderbooks () {
    let promises = defaultOptions.map(async (option) => {
      let optionAccountLayout = await getOptionAccount(option.pdaAccounts.optionAccountPda)
      return optionAccountLayout
    })

    const optionAccountLayouts = await Promise.all(promises)

    let _userOpenOrders: OrderInfoDisplay[] = []

    optionAccountLayouts.forEach((optionAccount) => {
      let long_orderbook = optionAccount.longOrderbook as Array<OrderInfo>;
      let short_orderbook = optionAccount.shortOrderbook as Array<OrderInfo>;

      let market = PythMarketsById[optionAccount.oracleAssetKey]


      if (long_orderbook.length > 0) {
        long_orderbook.forEach((order) => {
            if (order.trader.toBase58() === wallet.publicKey.toBase58()) {
              let openOrder = {
                oracleAssetKey: optionAccount.oracleAssetKey,
                market: market,
                expiry: optionAccount.expiry,
                strike: optionAccount.strike,
                strikeExponent: optionAccount.strikeExponent,
                trader: order.trader,
                quantity: order.quantity.toNumber(),
                price: order.price.toNumber(),
                isLong: order.isLong.toString(),
                isClose: order.isClose.toString(),
                orderTime: order.orderTime.toNumber()
              }
              _userOpenOrders.push(openOrder)
            }
        })
      }
      if (short_orderbook.length > 0) {
        short_orderbook.forEach((order) => {
            if (order.trader.toBase58() === wallet.publicKey.toBase58()) {
              let openOrder = {
                oracleAssetKey: optionAccount.oracleAssetKey,
                market: market,
                expiry: optionAccount.expiry,
                strike: optionAccount.strike,
                strikeExponent: optionAccount.strikeExponent,
                trader: order.trader,
                quantity: order.quantity.toNumber(),
                price: order.price.toNumber(),
                isLong: order.isLong.toString(),
                isClose: order.isClose.toString(),
                orderTime: order.orderTime.toNumber()
              }
              _userOpenOrders.push(openOrder)
            }
        })
      }
    })

    setSoceStore((state: SoceStore) => {
      state.userOpenOrders = _userOpenOrders;
    });

  }



  const handleCancelTrade = async(order) => {

    CancelTrade(
      escrowProgram,
      connection,
      wallet,
      order,
      selectedOption
    );

  }


  useEffect(() => {
   
    searchOrderbooks()

    return () => {}
  }, [])


  function tokenDisplay(order: OrderInfoDisplay): ReactElement {
    return (
      <div key={JSON.stringify(order)}>
        <div className="text-white bg-gradient-to-r from-gray-500 via-gray-600 to-gray-700 hover:bg-gradient-to-br focus:ring-1 focus:ring-gray-300 dark:focus:ring-gray-500 shadow-lg shadow-gray-500/50 dark:shadow-lg dark:shadow-gray-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-left mr-2">
          <li key={"a"}>Market: {order.market}</li>
          <li key={"b"}>Expiry: {new Date(order.expiry * 1000).toDateString()}</li>
          <li key={"c"}>Strike: {order.strike * Math.pow(10, order.strikeExponent)}</li>
          <li key={"d"}>Quantity: {order.quantity}</li>
          <li key={"e"}>Price: {order.price}</li>
          <li key={"f"}>isLong: {order.isLong}</li>
          <li key={"g"}>Order Time: {new Date(order.orderTime * 1000).toDateString()}</li>
          <button onClick={(event) => handleCancelTrade(order)}>Cancel</button>
        </div>
      </div>
    )
  }


  return (
    <div>
      {userOpenOrders.map((order) =>
        <div key={JSON.stringify(order)}>
          {tokenDisplay(order)}
        </div>
      )}
    </div>
  );
}

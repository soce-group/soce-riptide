import React from "react";
import SearchBar from "./MarketSearch";
import useSoceStore, { SoceStore } from "../../stores/useSoceStore";

export default function FavoritesSection() {
  const setSoceStore = useSoceStore((state: SoceStore) => state.set);
  return (
    <div className="flex bg-gray-300 h-20 gap-2 items-center justify-between px-3">
      <SearchBar />
      <div>
        <button
          type="button"
          onClick={() =>
            setSoceStore((state: SoceStore) => {
              console.log("MARKET SELECTED - SOLANA");
              state.selectedMarket = "SOL/USD";
              state.selectedOptionAccount = 3;
            })
          }
          className="text-white bg-gradient-to-r from-gray-500 via-gray-600 to-gray-700 hover:bg-gradient-to-br focus:ring-1 focus:ring-gray-300 dark:focus:ring-gray-500 shadow-lg shadow-gray-500/50 dark:shadow-lg dark:shadow-gray-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2"
        >
          SOL/USD
        </button>
        <button
          type="button"
          onClick={() =>
            setSoceStore((state: SoceStore) => {
              console.log("MARKET SELECTED - APPLE");
              state.selectedMarket = "AAPL/USD";
              state.selectedOptionAccount = 1;
            })
          }
          className="text-white bg-gradient-to-r from-gray-500 via-gray-600 to-gray-700 hover:bg-gradient-to-br focus:ring-1 focus:ring-gray-300 dark:focus:ring-gray-500 shadow-lg shadow-gray-500/50 dark:shadow-lg dark:shadow-gray-800/80 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2"
        >
          AAPL/USD
        </button>
      </div>
    </div>
  );
}

import { useWallet } from "@solana/wallet-adapter-react";
import React from "react";
import useSoceStore, {
  SoceStore,
  PythPriceData,
} from "../../stores/useSoceStore";

export default function BottomFooterPositionTabs() {
  // a handler to get data and may be format them the way we want to handle them
  const { wallet , connected} = useWallet();
  const consolidatedInfo = useSoceStore(
    (state: SoceStore) =>
      state.pythMarketProductDetails.consolidatedProductPriceDetails
  );
  const setSoceState = useSoceStore((state: SoceStore) => state.set);
  const showFloatingTab = useSoceStore(
    (state: SoceStore) => state.showFloatingTab
  );

  function setPriceDisplayData(e: any) {
    if (showFloatingTab) {
      setSoceState((state) => {
        state.showFloatingTab = false;
      });
      return;
    }
    const priceInfo: PythPriceData[] = [];
    consolidatedInfo.forEach((value, key) => {
      const priceData: PythPriceData = {
        symbol: key,
        price: value.price.price,
        confidence: value.price.confidence,
      };
      priceInfo.push(priceData);
    });

    setSoceState((state: SoceStore) => {
      state.selectedBottomTab = "prices";
      state.floatingElementData = priceInfo;
      state.showFloatingTab = true;
    });
  }

  return (
    <div className="flex bg-gray-400 flex-row justify-center -translate-y-1">
      <button
        type="button"
        name="long"
        disabled={!connected}
        onClick={() =>
          setSoceState((state: SoceStore) => (state.selectedBottomTab = "long"))
        }
        className="w-1/6 text-white bg-gradient-to-r from-indigo-500 via-indigo-600 to-indigo-700 hover:bg-gradient-to-br focus:ring-3 focus:ring-indigo-300 dark:focus:ring-indigo-500 shadow-lg shadow-indigo-500/50 dark:shadow-lg dark:shadow-indigo-800/80 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-3"
      >
        <span>Long Positions</span>
      </button>
      <button
        type="button"
        name="short"
        disabled={!connected}
        onClick={() =>
          setSoceState(
            (state: SoceStore) => (state.selectedBottomTab = "short")
          )
        }
        className="w-1/6 text-white bg-gradient-to-r from-indigo-500 via-indigo-600 to-indigo-700 hover:bg-gradient-to-br focus:ring-3 focus:ring-indigo-300 dark:focus:ring-indigo-500 shadow-lg shadow-indigo-500/50 dark:shadow-lg dark:shadow-indigo-800/80 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-3"
      >
        <span>Short Positions</span>
      </button>
      <button
        type="button"
        name="orders"
        disabled={!connected}
        onClick={() =>
          setSoceState(
            (state: SoceStore) => (state.selectedBottomTab = "orders")
          )
        }
        className="w-1/6 text-white bg-gradient-to-r from-indigo-500 via-indigo-600 to-indigo-700 hover:bg-gradient-to-br focus:ring-3 focus:ring-indigo-300 dark:focus:ring-indigo-500 shadow-lg shadow-indigo-500/50 dark:shadow-lg dark:shadow-indigo-800/80 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-3"
      >
        <span>All My Orders</span>
      </button>
      <button
        type="button"
        name="prices"
        onClick={setPriceDisplayData}
        className={
          showFloatingTab
            ? `w-1/6 text-white bg-gradient-to-r from-gray-500 via-gray-600 to-gray-700 hover:bg-gradient-to-br focus:ring-3 focus:ring-gray-300 dark:focus:ring-gray-500 shadow-lg shadow-gray-500/50 dark:shadow-lg dark:shadow-gray-800/80 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-3`
            : `w-1/6 text-white bg-gradient-to-r from-indigo-500 via-indigo-600 to-indigo-700 hover:bg-gradient-to-br focus:ring-3 focus:ring-indigo-300 dark:focus:ring-indigo-500 shadow-lg shadow-indigo-500/50 dark:shadow-lg dark:shadow-indigo-800/80 font-medium text-sm px-5 py-2.5 text-center mr-2 mb-3`
        }
      >
        <span>Latest Prices</span>
      </button>
    </div>
  );
}

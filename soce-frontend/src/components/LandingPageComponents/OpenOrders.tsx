import { useConnection } from "@solana/wallet-adapter-react";
import React, { useState } from "react";
import useSoceStore from "../../stores/useSoceStore";
import MatchedPositionTokens from "./MatchedPositionTokens";
import UnmatchedPositionTokens from "./UnmatchedPositionTokens";

export function OpenOrdersNew() {
  const [currentTab, changeCurrentTab] = React.useState("recent");
  const defaultOptions = useSoceStore((state) => state.defaultOptions);

  function changeTab(key: string) {
    changeCurrentTab(key);
  }
  const activeClasses =
    "w-1/3 bg-indigo-400 hover:bg-indigo-700 hover:text-white p-2 cursor-pointer self-start text-center";
  const inactiveClasses =
    "w-1/3 bg-gray-400 text-black hover:bg-gray-600 hover:text-white p-2 cursor-pointer self-start text-center";

  return (
    <div className="flex flex-col h-full">
      <div className="flex align-top items-center flex-row text-white gap-1 border border-indigo-100">
        <div
          onClick={(e) => changeTab("recent")}
          className={currentTab === "recent" ? activeClasses : inactiveClasses}
        >
          Up Trades
        </div>
        <div
          onClick={(e) => changeTab("all")}
          className={currentTab === "all" ? activeClasses : inactiveClasses}
        >
          Down Trades
        </div>
      </div>
      <div className="flex flex-col h-full">
        {currentTab === "recent" ? (
          defaultOptions && defaultOptions.allTransactionInformation ? (
            <UnmatchedPositionTokens />
          ) : null
        ) : null}
        {currentTab === "all" ? (
          defaultOptions && defaultOptions.allTransactionInformation ? (
            <MatchedPositionTokens />
          ) : null
        ) : null}
      </div>
    </div>
  );
}

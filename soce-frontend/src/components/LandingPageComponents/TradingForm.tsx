import React from "react";
import { OpenOrdersNew } from "./OpenOrders";
import TradingFormInput from "./TradingFormInput";
import TradingTopSectionContainer from "./TradingFormTopSection/TopSectionContainer";

export default function TradingFormComponent() {
  return (
    <div className="w-full max-h-full h-full bg-gray-400 flex flex-col justify-items-stretch gap-5 rounded-1xl overflow-hidden shadow-xl">
      <TradingTopSectionContainer />
      {/* <div className="px-10 pt-4 pb-2 bg-gray-400">
        <h1 className="text-2xl font-semibold text-gray-900 text-center">
          Trades for SOL/USD
        </h1>
        <h1 className="text-sm font-semibold text-indigo-600 text-center">
          Current Price - 149.22
        </h1>
      </div>
      <div className="px-3 h-48 pt-4 pb-3 bg-gray-200 flex flex-col items-stretch">
        <h1 className="text-xl font-semibold text-gray-900 text-center">
          Place Your Trade
        </h1>
        <TradingFormInput />
      </div> */}
      <div className="px-2 pt-2 pb-2 bg-gray-200 flex flex-col max-h-full h-full align-top justify-start">
        <OpenOrdersNew />
      </div>
    </div>
  );
}

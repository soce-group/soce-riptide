import React, { useEffect } from "react";
import { Menu, Transition, Combobox } from "@headlessui/react";
import { CheckIcon, SelectorIcon } from "@heroicons/react/solid";
import { ChevronDownIcon } from "@heroicons/react/solid";
import { Fragment } from "react";
import { defaultTestOptionAccountInfofinal } from "../../../instructions/instructions";
import useSoceStore, { SoceStore } from "../../../stores/useSoceStore";

export default function SelectFromDefaultOptions() {
  const [selected, setSelected] = React.useState("");
  const selectedDefaultMarket = useSoceStore(
    (state: SoceStore) => state.selectedMarket
  );
  const setSoceStore = useSoceStore((state: SoceStore) => state.set);
  const selectedDefaultOptionAccountId = useSoceStore(
    (state: SoceStore) => state.selectedOptionAccount
  );
  const [query, setQuery] = React.useState("");
  const [filteredOptions, setFilteredOptions] = React.useState<any[]>([]);

  useEffect(() => {
    const formattedValues = defaultTestOptionAccountInfofinal.map((d) => {
      const dateString = new Date(d.expiryUnixDate * 1000).toDateString();
      const optionStrike = (
        d.strikePriceRaw * Math.pow(10, d.strikeExponentRaw)
      )
        .toFixed(2)
        .toString();
      const formattedString = `${d.market} | ${dateString} | ${optionStrike}`;
      return { ...d, formattedString };
    });
    const options = formattedValues.filter(
      (d) => d.market === selectedDefaultMarket
    );
    const initialValue = formattedValues.filter(
      (a) => a.defaultOptionId === selectedDefaultOptionAccountId
    )[0].formattedString;
    setFilteredOptions(options);
    setSelected(initialValue);
  }, [selectedDefaultMarket]);

  return (
    <Combobox value={selected} onChange={(m) => setSelected(m)}>
      <div className="relative mt-1">
        <div className="relative w-full text-left bg-white rounded-lg shadow-md cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-gray-300 focus-visible:ring-offset-2 sm:text-sm overflow-hidden">
          <Combobox.Input
            className="bg-indigo-200 text-xl h-20 w-full border-none focus:ring-0 py-2 pl-3 pr-10 text-center leading-5 text-gray-900 shadow-2xl"
            displayValue={(m: string) => {
              return m;
            }}
            onChange={(event) => console.log("change in the options")}
          />
          <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
            <SelectorIcon
              className="w-5 h-5 text-gray-400"
              aria-hidden="true"
            />
          </Combobox.Button>
        </div>
        <Transition
          as={Fragment}
          leave="transition ease-in duration-100"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
          afterLeave={() => console.log("")}
        >
          <Combobox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
            {[].length === 0 && query !== "" ? (
              <div className="cursor-default select-none relative py-2 px-4 text-gray-700">
                Nothing found.
              </div>
            ) : (
              filteredOptions.map(({ defaultOptionId, formattedString }) => (
                <Combobox.Option
                  key={defaultOptionId}
                  className={({ active }) =>
                    `cursor-default select-none relative py-2 pl-10 pr-4 ${
                      active ? "text-white bg-gray-600" : "text-gray-900"
                    }`
                  }
                  onClick={(e) => console.log("selected", e)}
                  value={`${formattedString}`}
                >
                  {({ selected, active }) => {
                    if (selected) {
                      console.log(formattedString);
                      setSoceStore((state: SoceStore) => {
                        state.selectedOptionAccount = defaultOptionId;
                      });
                    }
                    return (
                      <>
                        <span
                          className={`block truncate ${
                            selected ? "font-medium" : "font-normal"
                          }`}
                        >
                          {`${formattedString}`}
                        </span>
                        {selected ? (
                          <span
                            className={`absolute inset-y-0 left-0 flex items-center pl-3 ${
                              active ? "text-white" : "text-gray-600"
                            }`}
                          >
                            <CheckIcon className="w-5 h-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    );
                  }}
                </Combobox.Option>
              ))
            )}
          </Combobox.Options>
        </Transition>
      </div>
    </Combobox>
  );
}

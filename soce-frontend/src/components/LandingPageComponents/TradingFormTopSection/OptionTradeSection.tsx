import React from "react";
import OpenNewTrade from "./../../../instructions/openNewTrade";
import useSoceStore from "../../../stores/useSoceStore";
import { useAnchorWallet } from "@solana/wallet-adapter-react";

export default function OptionTradeSection() {
  const [optionQuantity, setOptionQuantity] = React.useState<number>(0);
  const [optionPrice, setOptionPrice] = React.useState<number>(0);
  const isLong = React.useRef<boolean>(true);
  const isClose = React.useRef<boolean>(false);
  const [errorMessage, setErrorMessage] = React.useState<string>("");

  const anchorProvider = useSoceStore((state) => state.anchor.provider);

  const escrowProgram = useSoceStore((state) => state.anchor.escrowProgram);

  const wallet = useAnchorWallet();

  const selectedOptionAccount = useSoceStore(
    (state) => state.selectedOptionAccount
  )

  const soceStore = useSoceStore((state) => state);

  async function handleOrderSubmission(isLongFlag: boolean) {
    if (optionPrice < 0 || optionQuantity < 0) {
      setErrorMessage("Option Price or Quantity < 0");
    }
    if (!(optionPrice > 0 || optionPrice < 100)) {
      setErrorMessage("Option Price Between 1 to 99");
    }
    isLong.current = isLongFlag;
    const formValues = {
      amount: optionQuantity,
      price: optionPrice,
      isLong: isLong.current,
      isClose: isClose.current,
    };
    // No Errors
    setErrorMessage("");
    console.log("The values of the Form Messages", formValues);
    console.log("The Error Message", errorMessage);
    await handleOpenNewTrade(formValues);
  }

  const handleOpenNewTrade = async ({ amount, price, isLong, isClose }) => {
    // (alias) type TokenAccountsFilter = {
    //     mint: PublicKey;
    // } | {
    //     programId: PublicKey;
    // }

    // console.log(soceStore.defaultOptions.allTransactionInformation[0].publicKeys.longMintPdaKey.toBase58())
    // console.log(soceStore)
    // const accountsOwned = await anchorProvider.connection.getTokenAccountsByOwner(
    //   escrowProgram.programId,
    //   {
    //     mint: soceStore.defaultOptions.allTransactionInformation[0].publicKeys.longMintPdaKey,
    //     programId: escrowProgram.programId
    //   }
    // )

    // console.log(accountsOwned)
    console.log("Values Passed from the Form", amount, price, isLong, isClose);

    OpenNewTrade(
      anchorProvider,
      escrowProgram,
      wallet,
      soceStore,
      amount,
      price,
      isLong,
      isClose,
      selectedOptionAccount
    );
  };
  return (
    <div className="flex flex-col mt-5 bg-gray-300 p-4">
      <h1 className="text-2xl font-bold bg-gray-400 flex-1 w-full text-center">
        <span className="text-2xl font-bold bg-gray-400 text-indigo-800 flex-1 w-full text-center">
          Enter Your Bet
        </span>
      </h1>
      <div className="mt-6 flex flex-row gap-4">
        <div className="relative z-40">
          <input
            id="quantity"
            name="quantity"
            type="number"
            value={optionQuantity}
            onChange={(event) =>
              setOptionQuantity(parseFloat(event.target.value))
            }
            className="peer z-1 h-10 w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-gray-600"
            placeholder="1,234"
          />
          <label
            htmlFor="quantity"
            className="absolute left-2 -top-5 text-gray-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-5 peer-focus:text-gray-600 peer-focus:text-sm"
          >
            Quantity
          </label>
        </div>
        <div className="relative z-1">
          <input
            id="optionprice"
            name="optionprice"
            type="number"
            value={optionPrice}
            onChange={(event) => setOptionPrice(parseInt(event.target.value))}
            className="peer z-1 h-10 w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-gray-600"
            placeholder="35"
          />
          <label
            htmlFor="expiry"
            className="absolute left-2 -top-5 text-gray-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-5 peer-focus:text-gray-600 peer-focus:text-sm"
          >
            Option Price
          </label>
        </div>
      </div>
      <div
        onClick={(event) => handleOrderSubmission(true)}
        className="bg-gray-300 h-20 text-indigo-800 mt-3 flex flex-row gap-2 items-center hover:shadow-xl"
      >
        <button
          onClick={(event) => handleOrderSubmission(false)}
          className="text-3xl text-white bg-green-400 flex-1 h-full w-full text-center  hover:shadow-xl transition delay-500 active:animate-ping "
        >
          <span className="font-bold">Up</span>
        </button>
        <button className="text-3xl bg-red-500 text-white flex-1 w-full h-full text-center hover:shadow-xl transition delay-500 active:animate-ping ">
          <span className="font-bold">Down</span>
        </button>
      </div>
    </div>
  );
}

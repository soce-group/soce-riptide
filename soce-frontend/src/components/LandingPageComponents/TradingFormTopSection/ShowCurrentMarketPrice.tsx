import React from "react";

export default function CurrentMarketPriceInfo() {
  return (
    <div className="bg-gray-300 text-indigo-800 mt-3 flex flex-col gap-3 items-center p-3">
      <h1 className="text-2xl font-bold bg-gray-400 flex-1 w-full text-center">
        Current Market Price
      </h1>
      <h1 className="text-xl bg-indigo-800 text-white flex-1 w-full text-center">
        <span className="italic">USD : 150.53</span>
      </h1>
    </div>
  );
}

import React from "react";
import SelectDefaultOptions from "./SelectDefaultOptions";
import ShowCurrentMarketPrice from "./ShowCurrentMarketPrice";
import OptionTradeSection from "./OptionTradeSection";

export default function TradingTopSectionContainer() {
  return (
    <div className="flex w-full flex-col bg-gray-200 p-4">
      <SelectDefaultOptions />
      {/* <ShowCurrentMarketPrice /> */}
      <OptionTradeSection />
    </div>
  );
}

import React from "react";
import { useWallet } from "@solana/wallet-adapter-react";
import { WalletMultiButton } from "@solana/wallet-adapter-react-ui";
import { useCallback, useState } from "react";
import useLocalStorageState from "../../hooks/useLocalStorageState";
import MenuItem from "../CommonComponents/MenuItem";
import useSoceStore from "../../stores/useSoceStore";
import ConnectWalletButton from "../Wallet/ConnectWalletButton";
import { useTranslation } from "react-i18next";

import "./walletbutton.css";

export default function HeaderSection() {
  const { t } = useTranslation("common");
  const { wallet } = useWallet();
  const [showAccountsModal, setShowAccountsModal] = useState(false);

  const handleCloseAccounts = useCallback(() => {
    setShowAccountsModal(false);
  }, []);

  return (
    <div className="flex flex-row h-12 items-center justify-between bg-gray-100 opacity-90 relative z-30">
      <div className="flex flex-row pl-2 ml-2 items-center h-full">
        <img src="logo-home.svg" className="p-1 h-12 w-12 bg-white" alt="..." />
        <h1 className="pl-2 ml-2 text-gray-500 font-bold italic">
          SOCE Dem-Fi
        </h1>
      </div>
      <div className="flex flex-row justify-between gap-10 items-center">
        <a href="/Soce_Solana_Riptide.pdf" className="text-3xl h-full" download>
          🏄🏻
        </a>
        <WalletMultiButton />
      </div>
    </div>
  );
}

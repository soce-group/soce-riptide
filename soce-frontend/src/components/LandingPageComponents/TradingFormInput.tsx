import React, { useCallback, useEffect } from "react";
import useSoceStore, {
  SoceStore,
  PythPriceData,
} from "../../stores/useSoceStore";
import { useForm } from "react-hook-form";


import OpenNewTrade from "../../instructions/openNewTrade";
import { TokenAccountsFilter } from "@solana/web3.js";
import { useAnchorWallet } from "@solana/wallet-adapter-react";


export default function TradingFormInput() {
  const [optionQuantity, setOptionQuantity] = React.useState<number>(0)
  const [optionPrice, setOptionPrice] = React.useState<number>(0)
  const isLong  = React.useRef<boolean>(true)
  const isClose = React.useRef<boolean>(false)
  const [errorMessage, setErrorMessage] = React.useState<string>("")

  const anchorProvider = useSoceStore(
    (state) => state.anchor.provider
  );

  const escrowProgram = useSoceStore(
    (state) => state.anchor.escrowProgram
  );

  const wallet = useAnchorWallet();

  const soceStore = useSoceStore(
    (state) => state
  )

  const selectedOptionAccount = useSoceStore(
    (state) => state.selectedOptionAccount
  )

  async function handleOrderSubmission(isLongFlag:boolean) {
    if( optionPrice < 0 || optionQuantity < 0)  {
      setErrorMessage("Option Price or Quantity < 0")
    }
    if( !(optionPrice > 0 || optionPrice < 100))  {
      setErrorMessage("Option Price Between 1 to 99")
    }
    isLong.current = isLongFlag
    const formValues = {
      amount: optionQuantity,
      price: optionPrice,
      isLong: isLong.current,
      isClose: isClose.current
    }
    // No Errors
    setErrorMessage("")
    console.log("The values of the Form Messages", formValues)
    console.log("The Error Message", errorMessage)
    await handleOpenNewTrade(formValues)
  }

  const handleOpenNewTrade = async ({amount,price,isLong,isClose}) => {
    // (alias) type TokenAccountsFilter = {
    //     mint: PublicKey;
    // } | {
    //     programId: PublicKey;
    // }

    // console.log(soceStore.defaultOptions.allTransactionInformation[0].publicKeys.longMintPdaKey.toBase58())
    // console.log(soceStore)
    // const accountsOwned = await anchorProvider.connection.getTokenAccountsByOwner(
    //   escrowProgram.programId,
    //   {
    //     mint: soceStore.defaultOptions.allTransactionInformation[0].publicKeys.longMintPdaKey,
    //     programId: escrowProgram.programId
    //   }
    // )

    
    // console.log(accountsOwned)
    console.log("Values Passed from the Form", amount, price, isLong, isClose);

    OpenNewTrade(
      anchorProvider,
      escrowProgram,
      wallet,
      soceStore,
      amount,
      price,
      isLong,
      isClose,
      selectedOptionAccount
    )
  }

  return (
    <div
      className="mt-2 grid gap-1 grid-cols-2 grid-rows-2 place-items-center"
    >
      <div className="relative z-40">
        <input
          id="quantity"
          name="quantity"
          type="number"
          value={optionQuantity}
          onChange={(event) => setOptionQuantity(parseFloat(event.target.value))}
          className="peer px-2 z-1 h-10 w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-gray-600"
          placeholder="1,234"
        />
        <label
          htmlFor="quantity"
          className="absolute left-2 -top-5 text-gray-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-5 peer-focus:text-gray-600 peer-focus:text-sm"
        >
          Quantity
        </label>
      </div>
      <div className="relative z-1">
        <input
          id="optionprice"
          name="optionprice"
          type="number"
          value={optionPrice}
          onChange={(event) => setOptionPrice(parseInt(event.target.value))}
          className="peer px-2 z-1 h-10 w-full border-b-2 border-gray-300 text-gray-900 placeholder-transparent focus:outline-none focus:border-gray-600"
          placeholder="35"
        />
        <label
          htmlFor="expiry"
          className="absolute left-2 -top-5 text-gray-600 text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-400 peer-placeholder-shown:top-2 peer-focus:-top-5 peer-focus:text-gray-600 peer-focus:text-sm"
        >
          Option Price
        </label>
      </div>

      <input
        id="buyoption"
        name="buyoption"
        type="submit"
        value="Up"
        onClick={(event) => handleOrderSubmission(true)}
        className="mt-5 px-4 py-2 rounded bg-gray-500 hover:bg-gray-400 text-white font-semibold text-center block w-full focus:outline-none focus:ring focus:ring-offset-2 focus:ring-gray-500 focus:ring-opacity-80 cursor-pointer"
      />
      <input
        id="selloption"
        name="selloption"
        type="submit"
        value="Down"
        onClick={(event) => handleOrderSubmission(false)}
        className="mt-5 px-4 py-2 rounded bg-indigo-500 hover:bg-indigo-400 text-white font-semibold text-center block w-full focus:outline-none focus:ring focus:ring-offset-2 focus:ring-indigo-500 focus:ring-opacity-80 cursor-pointer"
      />
    </div>
  );
}

import React, { useEffect, useMemo } from "react";
// import useWallet from "./hooks/useWallet";
import useSoceStore, { SoceStore } from "./stores/useSoceStore";
// import { ConnectionProvider } from "./v1reference/contexts/connection";
// import { WalletProvider } from "./v1reference/contexts/wallet";

import { WalletAdapterNetwork } from "@solana/wallet-adapter-base";
import {
  ConnectionProvider,
  useConnection,
  useAnchorWallet,
  useWallet,
  WalletProvider,
} from "@solana/wallet-adapter-react";
import { WalletModalProvider } from "@solana/wallet-adapter-react-ui";

import {
  PhantomWalletAdapter,
  SlopeWalletAdapter,
} from "@solana/wallet-adapter-wallets";
import { clusterApiUrl, Commitment, PublicKey } from "@solana/web3.js";

import LandingPage from "./pages/LandingPage";
import useInterval from "./hooks/useInterval";
import InitializeSoceEnvironment from "./instructions/SoceBinaryOptionsInstructions";

import * as anchor from "@project-serum/anchor";
import { Escrow } from "./constants/escrow";
import idl from "./constants/escrow.json";


const SoceStateUpdater = () => {
  const { connection } = useConnection();
  const {
    wallet,
    connected,
    connect,
    disconnect,
    signAllTransactions,
    signTransaction,
    publicKey,
  } = useWallet();

  const opts = {
    preflightCommitment: "max" as Commitment,
  };

  const setSoceStore = useSoceStore((state: SoceStore) => state.set);

  const anchorWallet = useAnchorWallet();

  useEffect(() => {
    const provider = new anchor.Provider(
      connection,
      anchorWallet,
      opts
    );
    const escrowProgramId = new PublicKey(idl.metadata.address);
    const escrowProgram = new anchor.Program(
      idl as any as Escrow,
      escrowProgramId,
      provider
    );
    
    anchor.setProvider(provider);

    setSoceStore((state: SoceStore) => {
      state.anchor.escrowProgram = escrowProgram;
      state.anchor.escrowProgramId = escrowProgramId;
      state.anchor.provider = provider;
      state.wallet.current = wallet;
      state.wallet.connected = connected;
      state.wallet.connect = connect;
      state.wallet.disconnect = disconnect;
      state.wallet.signAllTransactions = signAllTransactions;
      state.wallet.signTransaction = signTransaction;
      state.wallet.publicKey = publicKey;
    });
  }, [connected, wallet, connection]);

  useEffect(() => {
    if (connection) {
      setSoceStore((state: SoceStore) => {
        state.connection.current = connection;
        state.connection.websocket = connection;
      });
    }
  }, [connection]);
  return null;
};

const PythPriceUpdater = () => {
  const fetchPythDetails = useSoceStore(
    (state) => state.actions.fetchPythOraclePrices
  );
  useInterval(() => {
    fetchPythDetails();
  }, 60 * 1000);
  return null;
};

export default function App() {
  // The network can be set to 'devnet', 'testnet', or 'mainnet-beta'.
  const network = WalletAdapterNetwork.Devnet;
  // const network = "devnet";

  // You can also provide a custom RPC endpoint.
  const endpoint = useMemo(() => clusterApiUrl(network), [network]);
  // const endpoint = "http://127.0.0.1:8899";

  const wallets = useMemo(
    () => [new PhantomWalletAdapter(), new SlopeWalletAdapter()],
    [network]
  );

  return (
    <ConnectionProvider endpoint={endpoint}>
      <WalletProvider wallets={wallets} autoConnect>
        <SoceStateUpdater />
        {/* <PythPriceUpdater /> */}
        <WalletModalProvider>
          <InitializeSoceEnvironment />
          <LandingPage />
        </WalletModalProvider>
      </WalletProvider>
    </ConnectionProvider>
  );
}

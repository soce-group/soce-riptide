import {
  SystemProgram,
  PublicKey,
  Keypair,
  Commitment,
  SYSVAR_RENT_PUBKEY,
  Connection,
  TransactionSignature,
  sendAndConfirmRawTransaction,
} from "@solana/web3.js";
import { TOKEN_PROGRAM_ID } from "@solana/spl-token";
// import * as anchor from "@project-serum/anchor";
// import { Escrow } from "../constants/escrow";
// import idl from "../constants/escrow.json";
import { AnchorWallet } from "@solana/wallet-adapter-react";
import { SendTxRequest } from "@project-serum/anchor/dist/cjs/provider";
import { Program, Provider } from "@project-serum/anchor";
import { Escrow } from "../constants/escrow";
import { BN } from "@project-serum/anchor";
import { idlAddress } from "@project-serum/anchor/dist/cjs/idl";

interface InstructionConnectionInfo {
  connectedWallet: AnchorWallet;
  currentConnection: Connection;
  anchorProvider: Provider;
  escrowProgram: Program<Escrow>;
  escrowProgramId: PublicKey;
}

interface IntializeOptionsParameters {
  ixConnection: InstructionConnectionInfo;
  defaultOptionId: number;
  oracleAssetInfo: string;
  expiryUnixDate: number;
  strikePriceRaw: number;
  strikeExponentRaw: number;
}
const defaultTestOptionAccountInfo = [
  {
    market: "Apple",
    oracleAssetInfo: "G89jkM5wFLpmnbvRbeePUumxsJyzoXaRfgBVjyx2CPzQ",
    strikePriceRaw: 17400000 * (1 + Math.random()),
    strikeExponentRaw: -5,
    expiryUnixDate: 1648065600 + Math.floor(Math.random() * 100),
  },
  {
    market: "Apple",
    oracleAssetInfo: "G89jkM5wFLpmnbvRbeePUumxsJyzoXaRfgBVjyx2CPzQ",
    strikePriceRaw: 17400000 * (2 + Math.random()),
    strikeExponentRaw: -5,
    expiryUnixDate: 1648065600 + Math.floor(Math.random() * 100),
  },
];

export const defaultTestOptionAccountInfofinal = [
  {
    defaultOptionId: 1,
    market: "AAPL/USD",
    oracleAssetInfo: "G89jkM5wFLpmnbvRbeePUumxsJyzoXaRfgBVjyx2CPzQ",
    strikePriceRaw: 17500000,
    strikeExponentRaw: -5,
    expiryUnixDate: 1648065602,
  },
  {
    defaultOptionId: 2,
    market: "AAPL/USD",
    oracleAssetInfo: "G89jkM5wFLpmnbvRbeePUumxsJyzoXaRfgBVjyx2CPzQ",
    strikePriceRaw: 13000000,
    strikeExponentRaw: -5,
    expiryUnixDate: 1648065602,
  },
  {
    defaultOptionId: 3,
    market: "SOL/USD",
    oracleAssetInfo: "3Mnn2fX6rQyUsyELYms1sBJyChWofzSNRoqYzvgMVz5E",
    strikePriceRaw: 10000000000,
    strikeExponentRaw: -8,
    expiryUnixDate: 1648008002,
  },
  {
    defaultOptionId: 4,
    market: "SOL/USD",
    oracleAssetInfo: "3Mnn2fX6rQyUsyELYms1sBJyChWofzSNRoqYzvgMVz5E",
    strikePriceRaw: 80000000000,
    strikeExponentRaw: -8,
    expiryUnixDate: 1648008002,
  },
];

export async function airDropSolana(
  connectionParams: InstructionConnectionInfo
) {
  const { currentConnection, connectedWallet, anchorProvider, escrowProgram } =
    connectionParams;

  const opts = {
    preflightCommitment: "processed" as Commitment,
  };

  await anchorProvider.connection.confirmTransaction(
    await anchorProvider.connection.requestAirdrop(
      connectedWallet.publicKey,
      1000000000
    ),
    "confirmed"
  );
}

export async function initializeOptionAccountInstruction({
  ixConnection,
  defaultOptionId,
  oracleAssetInfo,
  expiryUnixDate,
  strikePriceRaw,
  strikeExponentRaw = -8,
}: IntializeOptionsParameters) {
  const {
    connectedWallet,
    currentConnection,
    anchorProvider,
    escrowProgram,
    escrowProgramId,
  } = ixConnection;

  const optionTrader = new Keypair();
  const oracleAssetKey = new PublicKey(oracleAssetInfo);

  // Option Trade Details
  const expiry = new BN(expiryUnixDate);
  const strike = new BN(strikePriceRaw);
  const strikeExponent = new BN(strikeExponentRaw);

  // Set all the Option Account Seeds
  const optionAccountSeeds = [
    oracleAssetKey.toBuffer(),
    expiry.toArrayLike(Buffer, "le", 8),
    strike.toArrayLike(Buffer, "le", 8),
    strikeExponent.toTwos(32).toArrayLike(Buffer, "le", 4),
    Buffer.from("main"),
  ];

  const vaultAccountSeeds = [
    oracleAssetKey.toBuffer(),
    expiry.toArrayLike(Buffer, "le", 8),
    strike.toArrayLike(Buffer, "le", 8),
    strikeExponent.toTwos(32).toArrayLike(Buffer, "le", 4),
    Buffer.from("vault"),
  ];

  const longMintSeeds = [
    oracleAssetKey.toBuffer(),
    expiry.toArrayLike(Buffer, "le", 8),
    strike.toArrayLike(Buffer, "le", 8),
    strikeExponent.toTwos(32).toArrayLike(Buffer, "le", 4),
    Buffer.from("long"),
  ];

  const shortMintSeeds = [
    oracleAssetKey.toBuffer(),
    expiry.toArrayLike(Buffer, "le", 8),
    strike.toArrayLike(Buffer, "le", 8),
    strikeExponent.toTwos(32).toArrayLike(Buffer, "le", 4),
    Buffer.from("short"),
  ];

  // PDAs and Bumps
  const [optionAccountPda, optionAccountBump] =
    await PublicKey.findProgramAddress(optionAccountSeeds, escrowProgramId);
  const [vaultAccountPda, vaultAccountBump] =
    await PublicKey.findProgramAddress(vaultAccountSeeds, escrowProgramId);
  const [longMintPda, longMintBump] = await PublicKey.findProgramAddress(
    longMintSeeds,
    escrowProgramId
  );
  const [shortMintPda, shortMintBump] = await PublicKey.findProgramAddress(
    shortMintSeeds,
    escrowProgramId
  );

  // console.log("option trader: ", connectedWallet.publicKey.toBase58());
  // console.log("oracle_asset_key: ", oracleAssetKey.toBase58());
  // console.log("vault_pda: ", vaultAccountPda.toBase58());
  // console.log("oa_pda: ", optionAccountPda.toBase58());
  // console.log("long_mint_pda : ", longMintPda.toBase58());
  // console.log("short_mint_pda: ", shortMintPda.toBase58());
  // console.log("System Program", SystemProgram.programId.toBase58());
  // console.log("Long Bump", longMintBump);
  // console.log("Short Bump", shortMintBump);

  // set all this in some kind of a state so that

  const tx = await escrowProgram.transaction.initializeOption(
    oracleAssetKey,
    expiry,
    strike,
    strikeExponent,
    optionAccountBump,
    vaultAccountBump,
    longMintBump,
    shortMintBump,
    {
      accounts: {
        initializer: connectedWallet.publicKey,
        optionAccount: optionAccountPda,
        vaultAccount: vaultAccountPda,
        longMintAccount: longMintPda,
        shortMintAccount: shortMintPda,
        tokenProgram: TOKEN_PROGRAM_ID,
        rent: SYSVAR_RENT_PUBKEY,
        systemProgram: SystemProgram.programId,
      },
    }
  );
  // get the latest block hash
  tx.recentBlockhash = (await currentConnection.getLatestBlockhash()).blockhash;

  // assign the key pair to the
  tx.feePayer = connectedWallet.publicKey;

  return {
    defaultOptionId,
    programId: escrowProgramId,
    transaction: tx,
    optionTrader,
    oracleAssetKey,
    pdaAccounts: {
      optionAccountPda,
      vaultAccountPda,
      longMintPda,
      shortMintPda,
    },
    bumps: {
      optionAccountBump,
      longMintBump,
      vaultAccountBump,
      shortMintBump,
    },
    seeds: {
      optionAccountSeeds,
      vaultAccountSeeds,
      longMintSeeds,
      shortMintSeeds,
    },
    publicKeys: {
      optionTraderKey: optionTrader.publicKey,
      oracleAssetKey: oracleAssetKey,
      vaultAccountPdaKey: vaultAccountPda,
      optionAccountPdaKey: optionAccountPda,
      longMintPdaKey: longMintPda,
      shortMintPdaKey: shortMintPda,
    },
    publicKeysBase58: {
      optionTraderKeyb58: optionTrader.publicKey.toBase58(),
      oracleAssetKeyb58: oracleAssetKey.toBase58(),
      vaultAccountPdaKeyb58: vaultAccountPda.toBase58(),
      optionAccountPdaKeyb58: optionAccountPda.toBase58(),
      longMintPdaKeyb58: longMintPda.toBase58(),
      shortMintPdaKeyb58: shortMintPda.toBase58(),
      programIdKey58: escrowProgramId.toBase58(),
    },
    optionTradeDetailsTransformed: {
      strike,
      expiry,
      strikeExponent,
    },
    optionTradeDetailsRaw: {
      strike: strikePriceRaw,
      expiry: expiryUnixDate,
      strikeExponent: strikeExponentRaw,
    },
  };
}

function delay(delayInms) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve("done");
    }, delayInms);
  });
}

export async function seedDefaultOptions(
  ixConnection: InstructionConnectionInfo
) {
  const {
    currentConnection,
    connectedWallet,
    anchorProvider,
    escrowProgram,
    escrowProgramId,
  } = ixConnection;
  const opts = {
    preflightCommitment: "processed" as Commitment,
  };

  const prepareIntializeTransactions = defaultTestOptionAccountInfofinal.map(
    async (optionAccount) => {
      const { market, ...optionDetails } = optionAccount;
      console.log("Getting all the Option Details");
      const instructionParameters: IntializeOptionsParameters = {
        ixConnection,
        ...optionDetails,
      };
      await delay(300);
      return await initializeOptionAccountInstruction(instructionParameters);
    }
  );
  const defaultTransactions = await Promise.all(prepareIntializeTransactions);
  const allTransactions = defaultTransactions.map((t) => t.transaction);
  // TODO: non static initialization
  // const signedTransactions =
  //   await connectedWallet.signAllTransactions(
  //     allTransactions
  //   );

  const sigs: TransactionSignature[] = [];
  console.log("ALL TRANSACTION INFORMATION", allTransactions);
  // for (let k = 0; k < allTransactions.length; k += 1) {
  // //   TODO: non static initialization
  //   const tx = signedTransactions[k];
  //   const rawTx = tx.serialize();
  //   sigs.push(await sendAndConfirmRawTransaction(currentConnection, rawTx, opts));
  // }
  const finalizedTransactionReferences = await Promise.all(sigs);
  console.log("ALL FINALIZED TRANSACTIONS", finalizedTransactionReferences);
  return {
    allTransactionInformation: defaultTransactions,
    finalizedTransactionReferences,
  };
}

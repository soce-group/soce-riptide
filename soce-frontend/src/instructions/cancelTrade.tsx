import { BN, Program } from '@project-serum/anchor';
import { PublicKey, SystemProgram, SYSVAR_RENT_PUBKEY, sendAndConfirmRawTransaction, Commitment } from "@solana/web3.js";
import { token } from '@project-serum/anchor/dist/cjs/utils';
import { ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID, TOKEN_PROGRAM_ID } from "../utils/ids";
import { Escrow } from '../constants/escrow';
import { tokenHandler } from '../utils/tokens';
import { MatchedOrders, OrderInfo, OrderInfoDisplay } from '../@types/types';


export default async function CancelTrade(
  escrowProgram: Program<Escrow>,
  connection,
  wallet,
  selectedOrder: OrderInfoDisplay,
  selectedOption
) {

  const trader = selectedOrder.trader;
  const oracleAssetKey = selectedOrder.oracleAssetKey;
  const isLong = selectedOrder.isLong;
  const isClose = selectedOrder.isClose;
  const amount = new BN(selectedOrder.quantity);
  const price = new BN(selectedOrder.price);
  const option = selectedOption;
  const longMintPdaKey = option.publicKeys.longMintPdaKey;
  const shortMintPdaKey = option.publicKeys.shortMintPdaKey;
  const optionAccountPdaKey = option.publicKeys.optionAccountPdaKey;
  const vaultAccountPdaKey = option.publicKeys.vaultAccountPdaKey;
  const optionAccountBump = option.bumps.optionAccountBump;
  const vaultAccountBump = option.bumps.vaultAccountBump;
  const strike = new BN(option.optionTradeDetailsTransformed.strike);
  const expiry = new BN(option.optionTradeDetailsTransformed.expiry);
  const strikeExponent = new BN(option.optionTradeDetailsTransformed.strikeExponent);


  console.log("trader: ", trader)
  console.log("oracleAssetKey: ", oracleAssetKey)
  console.log("isLong: ", isLong)
  console.log("isClose: ", isClose)
  console.log("amount: ", amount)
  console.log("price: ", price)
  console.log("option: ", option)
  console.log("longMintPdaKey: ", longMintPdaKey)
  console.log("shortMintPdaKey: ", shortMintPdaKey)
  console.log("optionAccountPdaKey: ", optionAccountPdaKey)
  console.log("vaultAccountPdaKey: ", vaultAccountPdaKey)
  console.log("optionAccountBump: ", optionAccountBump)
  console.log("vaultAccountBump: ", vaultAccountBump)
  console.log("strike: ", strike)
  console.log("expiry: ", expiry)
  console.log("strikeExponent: ", strikeExponent)

  
  const tx = await escrowProgram.transaction.cancelTrade(
    oracleAssetKey,
    optionAccountPdaKey,
    optionAccountBump,
    strike,
    strikeExponent,
    expiry,
    amount,
    price,
    isLong,
    isClose,
    vaultAccountBump,
    {
      accounts: {
        trader: trader,
        optionAccount: optionAccountPdaKey,
        vaultAccount: vaultAccountPdaKey,
        associatedTokenProgram: ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID,
        tokenProgram: TOKEN_PROGRAM_ID,
        rent: SYSVAR_RENT_PUBKEY,
        systemProgram: SystemProgram.programId
      },
    },
  )
  tx.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  tx.feePayer = wallet.publicKey;

  const signTxResult =
    await wallet.signTransaction(
      tx
    );

  const opts = {
    preflightCommitment: "confirmed" as Commitment,
  };
  
  let tradeTxResult = 
    await sendAndConfirmRawTransaction(
      connection,
      tx.serialize(),
      opts
    )
  
  console.log("tradeTxResult: ", tradeTxResult);

}
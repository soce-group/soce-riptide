import React, { useEffect } from "react";
import { seedDefaultOptions, airDropSolana } from "./instructions";
import * as anchor from "@project-serum/anchor";
import {
  useAnchorWallet,
  useConnection,
  useWallet,
} from "@solana/wallet-adapter-react";

import useSoceStore, { SoceStore } from "../stores/useSoceStore";

export default function IntializeOptionsAccounts() {
  const currentConnection = useConnection().connection;
  const connectedWallet = useAnchorWallet();
  const setSoceStore = useSoceStore((state: SoceStore) => state.set);
  const { connected } = useWallet();

  const anchorProvider = useSoceStore(
    (state) => state.anchor.provider
  );

  const escrowProgramId = useSoceStore(
    (state) => state.anchor.escrowProgramId
  );
  const escrowProgram = useSoceStore(
    (state) => state.anchor.escrowProgram
  );

  useEffect(() => {
    async function generateDefaultOptions() {
      console.log("generateDefaultOptions")
      console.log(escrowProgramId)
      const defaultOptions = await seedDefaultOptions({
        connectedWallet,
        currentConnection,
        anchorProvider,
        escrowProgram,
        escrowProgramId,
      });
      console.log("DEFAULT OPTIONS", defaultOptions);
      setSoceStore((state: SoceStore) => {
        state.defaultOptions = defaultOptions;
      });
    }
    if (connected) {
      generateDefaultOptions();
    }
  }, [connected, connectedWallet, currentConnection, setSoceStore]);

  return null;
}

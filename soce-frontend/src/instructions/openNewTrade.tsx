import { BN, Program, web3 } from '@project-serum/anchor';
import { PublicKey, SystemProgram, SYSVAR_RENT_PUBKEY, sendAndConfirmRawTransaction, Commitment } from "@solana/web3.js";
import { nu64 } from "@solana/buffer-layout";
import { token } from '@project-serum/anchor/dist/cjs/utils';
import { ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID, TOKEN_PROGRAM_ID } from "../utils/ids";
import { AccountLayout } from "@solana/spl-token";
import { Escrow } from '../constants/escrow';
import { tokenHandler } from '../utils/tokens';
import { MatchedOrders, OrderInfo } from '../@types/types';


export default async function OpenNewTrade(
  anchorProvider,
  escrowProgram: Program<Escrow>,
  wallet,
  state,
  _amount: number,
  _price: number,
  _isLong: boolean,
  _isClose: boolean,
  selectedOptionAccount: number
) {
  const trader = wallet.publicKey;

  const isLong = _isLong;
  const isClose = _isClose;
  const amount = new BN(_amount);
  const price = new BN(_price);

  const option = state.defaultOptions.allTransactionInformation[selectedOptionAccount];
  const longMintPdaKey = option.publicKeys.longMintPdaKey;
  const shortMintPdaKey = option.publicKeys.shortMintPdaKey;
  const optionAccountPdaKey = option.publicKeys.optionAccountPdaKey;
  const vaultAccountPdaKey = option.publicKeys.vaultAccountPdaKey;
  const oracleAssetKey = option.publicKeys.oracleAssetKey;
  const optionAccountBump = option.bumps.optionAccountBump;
  const longMintBump = option.bumps.longMintBump;
  const shortMintBump = option.bumps.shortMintBump;
  const vaultAccountBump = option.bumps.vaultAccountBump;
  const strike = option.optionTradeDetailsTransformed.strike;
  const expiry = option.optionTradeDetailsTransformed.expiry;
  const strikeExponent = option.optionTradeDetailsTransformed.strikeExponent;
  const connection = state.connection.current;

  console.log("trader", trader);
  console.log("selectedOptionAccount", selectedOptionAccount);
  console.log("isLong", isLong);
  console.log("isClose", isClose);
  console.log("option", option);
  console.log("longMintPdaKey", longMintPdaKey);
  console.log("shortMintPdaKey", shortMintPdaKey);
  console.log("optionAccountPdaKey", optionAccountPdaKey);
  console.log("vaultAccountPdaKey", vaultAccountPdaKey);
  console.log("oracleAssetKey", oracleAssetKey);
  console.log("optionAccountBump", optionAccountBump);
  console.log("longMintBump", longMintBump);
  console.log("shortMintBump", shortMintBump);
  console.log("strike", strike);
  console.log("expiry", expiry);
  console.log("strikeExponent", strikeExponent);
  console.log("amount", amount);
  console.log("price", price);
  console.log("connection", connection);





  const traderMintSeeds = [
    trader.toBuffer()
  ]

  const [ traderMintPda, traderMintBump ] = await PublicKey.findProgramAddress(
    traderMintSeeds,
    escrowProgram.programId
  );

  const traderlongMintPdaKey = await token.associatedAddress({
    mint: longMintPdaKey,
    owner: traderMintPda
  });

  const tradershortMintPdaKey = await token.associatedAddress({
    mint: shortMintPdaKey,
    owner: traderMintPda
  });

  let beforeOptionAccountLayout = await escrowProgram.account.optionAccount.fetch(
    optionAccountPdaKey
  )

  console.log("optionAccountLayout: ", beforeOptionAccountLayout)



  let before_long_orderbook = beforeOptionAccountLayout.longOrderbook as Array<OrderInfo>;
  let before_short_orderbook = beforeOptionAccountLayout.shortOrderbook as Array<OrderInfo>;

  if (before_long_orderbook.length + before_short_orderbook.length > 12 ) {
    console.log('orderbooks full')
    console.log('before_long_orderbook.length: ', before_long_orderbook.length)
    console.log('before_short_orderbook.length: ', before_short_orderbook.length)
    return
  } else {
    console.log('before_long_orderbook.length: ', before_long_orderbook.length)
    console.log('before_short_orderbook.length: ', before_short_orderbook.length)
  }
  
  const tx = await escrowProgram.transaction.openNewTrade(
    oracleAssetKey,
    optionAccountPdaKey,
    optionAccountBump,
    strike,
    strikeExponent,
    expiry,
    amount,
    price,
    isLong,
    isClose,
    traderMintBump,
    {
      accounts: {
        trader: trader,
        traderMintPda: traderMintPda,
        traderLongMintAccount: traderlongMintPdaKey,
        traderShortMintAccount: tradershortMintPdaKey,
        optionAccount: optionAccountPdaKey,
        vaultAccount: vaultAccountPdaKey,
        longMintAccount: longMintPdaKey,
        shortMintAccount: shortMintPdaKey,
        associatedTokenProgram: ASSOCIATED_TOKEN_ACCOUNT_PROGRAM_ID,
        tokenProgram: TOKEN_PROGRAM_ID,
        rent: SYSVAR_RENT_PUBKEY,
        systemProgram: SystemProgram.programId
      },
    },
  );
  tx.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
  tx.feePayer = wallet.publicKey;

  const signTxResult =
    await wallet.signTransaction(
      tx
    );

  const opts = {
    preflightCommitment: "confirmed" as Commitment,
  };
  
  let tradeTxResult = 
    await sendAndConfirmRawTransaction(
      connection,
      tx.serialize(),
      opts
    )
  
  console.log("tradeTxResult: ", tradeTxResult);

  let optionAccountLayout = await escrowProgram.account.optionAccount.fetch(
    optionAccountPdaKey
  )

  console.log("optionAccountLayout: ", optionAccountLayout)


  let long_orderbook = optionAccountLayout.longOrderbook as Array<OrderInfo>;
  let short_orderbook = optionAccountLayout.shortOrderbook as Array<OrderInfo>;

  long_orderbook.sort((a, b) =>
    a.price.toNumber() - b.price.toNumber()
  );

  short_orderbook.sort((a, b) =>
    a.price.toNumber() - b.price.toNumber()
  );
  console.log("orderbooks before match")
  console.log("long_orderbook", long_orderbook)
  console.log("short_orderbook", short_orderbook)
  const top_long_orderbook = long_orderbook.pop()
  const top_short_orderbook = short_orderbook.pop()

  if (!top_long_orderbook) {
    console.log("no top_long_orderbook found")
    return
  }
  console.log("top_long_orderbook: ", top_long_orderbook)

  if (!top_short_orderbook) {
    console.log("no top_short_orderbook found")
    return
  }
  console.log("top_short_orderbook: ", top_short_orderbook)

  const matched_orders: MatchedOrders = {
    long: top_long_orderbook,
    short: top_short_orderbook
  }

  if ( top_long_orderbook.price.toNumber() 
      + top_short_orderbook.price.toNumber() > 99
  ) {
    console.log("found match")
          
    const matched_long_trader_mint_seeds = [
      matched_orders.long.trader.toBuffer()
    ]

    const matched_short_trader_mint_seeds = [
      matched_orders.short.trader.toBuffer()
    ]

    const [ 
      matched_long_trader_mint_pda,
      matched_long_trader_mint_bump
    ] = await PublicKey.findProgramAddress(
        matched_long_trader_mint_seeds,
        escrowProgram.programId
    );
    
    const [
      matched_short_trader_mint_pda,
      matched_short_trader_mint_bump
    ] = await PublicKey.findProgramAddress(
        matched_short_trader_mint_seeds,
        escrowProgram.programId
    );

    console.log("token balances before match")

    await tokenHandler(
      connection,
      matched_long_trader_mint_pda,
      longMintPdaKey
    )

    await tokenHandler(
      connection,
      matched_long_trader_mint_pda,
      shortMintPdaKey
    )

    await tokenHandler(
      connection,
      matched_short_trader_mint_pda,
      longMintPdaKey
    )
    
    await tokenHandler(
      connection,
      matched_short_trader_mint_pda,
      shortMintPdaKey
    )

    let matched_qty = new BN(0);
    if ( matched_orders.long.quantity > matched_orders.short.quantity ) {
      matched_qty = matched_orders.short.quantity;
    } else {
      matched_qty = matched_orders.long.quantity;
    };

    console.log("submitting matchOrder:")
    console.log("matched_qty.price", matched_qty.toNumber())
    console.log("top_long_orderbook.price", top_long_orderbook.price.toNumber())
    console.log("top_short_orderbook.price", top_short_orderbook.price.toNumber())
    console.log("matched_orders.long.trader: ", matched_orders.long.trader.toBase58())
    console.log("matched_orders.long.price: ", matched_orders.long.price.toNumber())
    console.log("matched_orders.long.quantity: ", matched_orders.long.quantity.toNumber())
    console.log("matched_orders.short.trader: ", matched_orders.short.trader.toBase58())
    console.log("matched_orders.short.price: ", matched_orders.short.price.toNumber())
    console.log("matched_orders.short.quantity: ", matched_orders.short.quantity.toNumber())

    const tx = await escrowProgram.transaction.matchOrder(
      oracleAssetKey,
      longMintBump,
      shortMintBump,
      expiry,
      strike,
      strikeExponent,
      matched_qty,
      matched_long_trader_mint_bump,
      matched_short_trader_mint_bump,
      vaultAccountBump,
      {
        accounts: {
          optionAccount: optionAccountPdaKey,
          longTrader: matched_orders.long.trader,
          longTraderMintPda: matched_long_trader_mint_pda,
          longTraderLongMintAccount: matched_orders.long.traderLongMintAccount,
          longTraderShortMintAccount: matched_orders.long.traderShortMintAccount,
          shortTrader: matched_orders.short.trader,
          shortTraderMintPda: matched_short_trader_mint_pda,
          shortTraderLongMintAccount: matched_orders.short.traderLongMintAccount,
          shortTraderShortMintAccount: matched_orders.short.traderShortMintAccount,
          vaultAccount: vaultAccountPdaKey,
          longMintAccount: longMintPdaKey,
          shortMintAccount: shortMintPdaKey,
          tokenProgram: TOKEN_PROGRAM_ID,
          systemProgram: SystemProgram.programId
        },
      },
    )
    tx.recentBlockhash = (await connection.getLatestBlockhash()).blockhash;
    tx.feePayer = wallet.publicKey;
  
    const signTxResult =
      await wallet.signTransaction(
        tx
      );
  
    const opts = {
      preflightCommitment: "confirmed" as Commitment,
    };
    
    let tradeTxResult = 
      await sendAndConfirmRawTransaction(
        connection,
        tx.serialize(),
        opts
      )
    
    console.log("tradeTxResult: ", tradeTxResult);

    console.log("token balances after match")
    await tokenHandler(
      connection,
      matched_long_trader_mint_pda,
      longMintPdaKey
    )

    await tokenHandler(
      connection,
      matched_long_trader_mint_pda,
      shortMintPdaKey
    )

    await tokenHandler(
      connection,
      matched_short_trader_mint_pda,
      longMintPdaKey
    )
    
    await tokenHandler(
      connection,
      matched_short_trader_mint_pda,
      shortMintPdaKey
    )

  } else {
    console.log("no match found")
    console.log("long_orderbook", long_orderbook)
    console.log("short_orderbook", short_orderbook)
  }

}
import { AccountInfo, PublicKey, Transaction } from '@solana/web3.js'
import { Event } from '@project-serum/serum/lib/queue'
import { BN } from '@project-serum/anchor'

export interface Token {
  chainId: number // 101,
  address: string // 'EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v',
  symbol: string // 'USDC',
  name: string // 'Wrapped USDC',
  decimals: number // 6,
  logoURI: string // 'https://raw.githubusercontent.com/solana-labs/token-list/main/assets/mainnet/BXXkv6z8ykpG1yuvUDPgh732wzVHB69RnB9YgSYh3itW/logo.png',
  tags: string[] // [ 'stablecoin' ]
}
export interface EndpointInfo {
  name: string
  url: string
  websocket: string
  custom: boolean
}


export interface OrderInfo {
  trader: PublicKey,
  traderMintPda: PublicKey,
  traderLongMintAccount: PublicKey,
  traderShortMintAccount: PublicKey,
  quantity: BN,
  price: BN,
  isLong: Boolean,
  isClose: Boolean,
  orderTime: BN
}

export interface MatchedOrders {
  long: OrderInfo,
  short: OrderInfo,
}

export interface ParsedToken {
  mint: PublicKey,
  owner: PublicKey,
  amount: number,
  closeAuthority: PublicKey,
}

export interface OrderInfoDisplay {
  oracleAssetKey: PublicKey,
  market: string,
  expiry: number,
  strike: number,
  strikeExponent: number,
  trader: PublicKey,
  quantity: number,
  price: number,
  isLong: string,
  isClose: string,
  orderTime: number
}
export interface OptionTokenAccounts {
  oracleAssetKey: PublicKey,
  expiry: number,
  strike: number,
  strikeExponent: number,
  userLongTokenAccount: ParsedToken,
  userShortTokenAccount: ParsedToken
}

export interface ChartTradeType {
  market: string
  size: number
  price: number
  orderId: string
  time: number
  side: string
  feeCost: number
  marketAddress: string
}

export declare type Cluster = 'devnet' | 'mainnet' | 'localnet' | 'testnet';

export interface OracleConfig {
    symbol: string;
    publicKey: PublicKey;
}
export interface TokenConfig {
    symbol: string;
    mintKey: PublicKey;
    decimals: number;
    rootKey: PublicKey;
    nodeKeys: PublicKey[];
}

//# sourceMappingURL=config.d.ts.map
export interface AccountInfoList {
  [key: string]: AccountInfo<Buffer>
}

export interface WalletToken {
  account: TokenAccount
  config: TokenConfig
  uiBalance: number
}

export interface Orderbook {
  bids: number[][]
  asks: number[][]
}

export interface Token {
  chainId: number // 101,
  address: string // 'EPjFWdd5AufqSSqeM2qN1xzybapC8G4wEGGkZwyTDt1v',
  symbol: string // 'USDC',
  name: string // 'Wrapped USDC',
  decimals: number // 6,
  logoURI: string // 'https://raw.githubusercontent.com/solana-labs/token-list/main/assets/mainnet/BXXkv6z8ykpG1yuvUDPgh732wzVHB69RnB9YgSYh3itW/logo.png',
  tags: string[] // [ 'stablecoin' ]
}

export interface MarketInfo {
  address: PublicKey
  name: string
  programId: PublicKey
  deprecated: boolean
  quoteLabel?: string
  baseLabel?: string
}

export interface TokenAccount {
  pubkey: PublicKey
  account: AccountInfo<Buffer> | null
  effectiveMint: PublicKey
}

export interface Trade extends Event {
  side: string
  price: number
  feeCost: number
  size: number
}

// export interface BalancesBase {
//   key: string
//   symbol: string
//   wallet?: number | null | undefined
//   orders?: number | null | undefined
//   openOrders?: OpenOrders | null | undefined
//   unsettled?: number | null | undefined
// }

// export interface Balances extends BalancesBase {
//   market?: Market | null | undefined
//   deposits?: I80F48 | null | undefined
//   borrows?: I80F48 | null | undefined
//   net?: I80F48 | null | undefined
//   value?: I80F48 | null | undefined
//   depositRate?: I80F48 | null | undefined
//   borrowRate?: I80F48 | null | undefined
// }

export interface EndpointInfo {
  name: string
  url: string
  websocket: string
  custom: boolean
}

export interface SelectedTokenAccounts {
  [tokenMint: string]: string
}

export const DEFAULT_PUBLIC_KEY = new PublicKey(
  '11111111111111111111111111111111'
)

export interface WalletAdapter {
  publicKey: PublicKey | null | undefined,
  autoApprove: boolean
  connected: boolean
  signTransaction: (transaction: Transaction) => Promise<Transaction|null>
  signAllTransactions: (transaction: Transaction[]) => Promise<Transaction[]| null>
  connect: () => any
  disconnect: () => any
  on(event: string, fn: () => void): this
}

export interface WalletToken {
  account: TokenAccount
  config: TokenConfig
  uiBalance: number
}

export interface Orderbook {
  bids: number[][]
  asks: number[][]
}

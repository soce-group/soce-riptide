import React from "react";
import SearchAndFavoritesSection from "../components/LandingPageComponents/FavoritesSection";
import MainSoceContent from "../components/LandingPageComponents/MainSoceContent";
import HeaderSection from "../components/LandingPageComponents/HeaderSection";
import BottomFooterPositionTabs from "../components/LandingPageComponents/BottomFooterTabs";
// import FloatingSlideOver from "../components/Trial/FloatingSlideOver";

export default function LandingPage() {
  return (
    <div className="flex gap-1 flex-col h-screen w-screen">
      <HeaderSection />
      <SearchAndFavoritesSection />
      <MainSoceContent />
      <BottomFooterPositionTabs />
      {/* <FloatingSlideOver /> */}
    </div>
  );
}

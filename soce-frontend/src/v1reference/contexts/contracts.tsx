import React, {
  useCallback,
  useContext,
  useState
} from "react";
import type { Moment } from "moment";

// TODO: Apply sane defaults instead of undefined
// Needed for the PublicKey definitions in the SettleCollectButton
export interface MatchableContract {
  id: number,
  symbol: string | undefined,
  symbol_key: string,
  expiry: Moment | null | undefined | string,
  strike: number | undefined,
  seller_id: string | undefined,
  seller_percent: number | undefined,
  seller_volume: number | undefined,
  buyer_id: string | undefined,
  buyer_percent: number | undefined,
  buyer_volume: number | undefined,
  is_settled: boolean,
  is_collected: boolean,
  pool_account: string | undefined,
  long_token_mint: string | undefined,
  short_token_mint: string | undefined,
  price_key: string | undefined,
  escrow_account: string | undefined,
  escrow_authority_account: string | undefined,
  escrow_mint_account: string | undefined,
}

export interface MatchableContractProps {
  contract: MatchableContract,
  matchableContracts: MatchableContract[] | undefined,
  selectContract: ((contract: MatchableContract) => void)
}

const MatchableContractContext = React.createContext<MatchableContractProps>({
  contract: {} as MatchableContract,
  matchableContracts: [{} as MatchableContract],
  selectContract() {}
});

export function MatchableContractProvider({
    children = null as any,
    contract = {} as MatchableContract,
    matchableContracts = [] as MatchableContract[],
    selectContract = (contract: MatchableContract) => {}
}) {
  
  return (
    <MatchableContractContext.Provider
      value={{
        contract,
        matchableContracts,
        selectContract
      }}
    >
      {children}
    </MatchableContractContext.Provider>
  );
}

export function useMatchableContract() {
  const { 
    contract,
    matchableContracts,
    selectContract,
  } = useContext(MatchableContractContext);
  return {
    contract,
    matchableContracts,
    selectContract,
  };
}

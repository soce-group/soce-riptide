import { PhantomWalletAdapter } from './phantom'
import { SolflareWalletAdapter } from './solflare'
import { SolletExtensionAdapter } from './sollet-extension'
import { SlopeWalletAdapter } from './slope'
import { BitpieWalletAdapter } from './bitpie'
import { HuobiWalletAdapter } from './huobi'
import { GlowWalletAdapter } from './glow'

const ASSET_URL =
  'https://cdn.jsdelivr.net/gh/solana-labs/oyster@main/assets/wallets'

export const WALLET_PROVIDERS = [
  {
    name: 'Phantom',
    url: 'https://www.phantom.app',
    icon: `https://www.phantom.app/img/logo.png`,
    adapter: PhantomWalletAdapter,
  },
  {
    name: 'Solflare',
    url: 'https://solflare.com',
    icon: `${ASSET_URL}/solflare.svg`,
    adapter: SolflareWalletAdapter,
  },
  {
    name: 'Sollet.io',
    url: 'https://www.sollet.io',
    icon: `${ASSET_URL}/sollet.svg`,
  },
  {
    name: 'Sollet Extension',
    url: 'https://www.sollet.io/extension',
    icon: `${ASSET_URL}/sollet.svg`,
    adapter: SolletExtensionAdapter as any,
  }
]
export const DEFAULT_PROVIDER = WALLET_PROVIDERS[0]

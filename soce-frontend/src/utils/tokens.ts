import { Connection, PublicKey } from '@solana/web3.js'
import { AccountLayout } from "@solana/spl-token";
import { nu64 } from "@solana/buffer-layout";
import { web3 } from "@project-serum/anchor";
import { ParsedToken } from '../@types/types';




export async function tokenHandler (
  connection: Connection,
  userMintAccount: PublicKey,
  mint: PublicKey,
): Promise<any> {
  const tokenAccount = await connection.getTokenAccountsByOwner(
    userMintAccount,
    {
      mint: mint,
      encoding: "jsonParsed"
    } as web3.TokenAccountsFilter
  )
  if ( tokenAccount.value.length == 0 ) { return }

  let token_account = tokenAccount.value[0].account as web3.AccountInfo<Buffer>
  let account_info = AccountLayout.decode(token_account.data)
  let account_mint = new PublicKey(account_info.mint)
  let account_owner = new PublicKey(account_info.owner)
  let account_amount_nu64 = nu64().decode(Buffer.from(account_info.amount, 'hex'))
  let account_closeAuthority = new PublicKey(account_info.closeAuthority)

  let parsedTokenAccount: ParsedToken = {
    mint: account_mint,
    owner: account_owner,
    amount: account_amount_nu64,
    closeAuthority: account_closeAuthority,
  }

  return parsedTokenAccount
}


import useSoceStore from '../stores/useSoceStore'

export type Notification = {
  type: 'success' | 'info' | 'error' | 'confirm'
  title: string
  description?: null | string
  txid?: string
  show: boolean
  id: number
}

export function notify(newNotification: {
  type?: 'success' | 'info' | 'error' | 'confirm'
  title: string
  description?: string
  txid?: string
}) {
  const setSoceStore = useSoceStore.getState().set
  const notifications = useSoceStore.getState().notifications
  const lastId = useSoceStore.getState().notificationIdCounter
  const newId = lastId + 1

  const newNotif: Notification = {
    id: newId,
    type: 'success',
    show: true,
    description: null,
    ...newNotification,
  }

  setSoceStore((state: any) => {
    state.notificationIdCounter = newId
    state.notifications = [...notifications, newNotif]
  })
}

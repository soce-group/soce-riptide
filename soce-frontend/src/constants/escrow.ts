export type Escrow = {
  "version": "0.1.0",
  "name": "escrow",
  "instructions": [
    {
      "name": "initializeOption",
      "accounts": [
        {
          "name": "initializer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "openNewTrade",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "traderMintPda",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "optionAccount",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "quantity",
          "type": "u64"
        },
        {
          "name": "price",
          "type": "u64"
        },
        {
          "name": "isLong",
          "type": "bool"
        },
        {
          "name": "isClose",
          "type": "bool"
        },
        {
          "name": "traderMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "cancelTrade",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "optionAccount",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "quantity",
          "type": "u64"
        },
        {
          "name": "price",
          "type": "u64"
        },
        {
          "name": "isLong",
          "type": "bool"
        },
        {
          "name": "isClose",
          "type": "bool"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "matchOrder",
      "accounts": [
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longTrader",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "longTraderMintPda",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "longTraderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longTraderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortTrader",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortTraderMintPda",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortTraderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortTraderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "amount",
          "type": "u64"
        },
        {
          "name": "longTraderMintBump",
          "type": "u8"
        },
        {
          "name": "shortTraderMintBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "settleOption",
      "accounts": [
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "oracleAssetInfo",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "oracleAssetPriceInfo",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "collectOption",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "traderMintPda",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "amount",
          "type": "u64"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        },
        {
          "name": "traderMintBump",
          "type": "u8"
        }
      ]
    }
  ],
  "accounts": [
    {
      "name": "optionAccount",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "optionLister",
            "type": "publicKey"
          },
          {
            "name": "oracleAssetKey",
            "type": "publicKey"
          },
          {
            "name": "vaultAccount",
            "type": "publicKey"
          },
          {
            "name": "longMint",
            "type": "publicKey"
          },
          {
            "name": "shortMint",
            "type": "publicKey"
          },
          {
            "name": "expiry",
            "type": "u64"
          },
          {
            "name": "strike",
            "type": "u64"
          },
          {
            "name": "strikeExponent",
            "type": "i32"
          },
          {
            "name": "oaBump",
            "type": "u8"
          },
          {
            "name": "vaultBump",
            "type": "u8"
          },
          {
            "name": "longMintBump",
            "type": "u8"
          },
          {
            "name": "shortMintBump",
            "type": "u8"
          },
          {
            "name": "longOrderbook",
            "type": {
              "vec": {
                "defined": "OrderInfo"
              }
            }
          },
          {
            "name": "shortOrderbook",
            "type": {
              "vec": {
                "defined": "OrderInfo"
              }
            }
          },
          {
            "name": "isSettled",
            "type": "bool"
          },
          {
            "name": "winningMint",
            "type": "publicKey"
          },
          {
            "name": "isTie",
            "type": "bool"
          }
        ]
      }
    }
  ],
  "types": [
    {
      "name": "OrderInfo",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "trader",
            "type": "publicKey"
          },
          {
            "name": "traderMintBump",
            "type": "u8"
          },
          {
            "name": "traderLongMintAccount",
            "type": "publicKey"
          },
          {
            "name": "traderShortMintAccount",
            "type": "publicKey"
          },
          {
            "name": "quantity",
            "type": "u64"
          },
          {
            "name": "price",
            "type": "u64"
          },
          {
            "name": "isLong",
            "type": "bool"
          },
          {
            "name": "isClose",
            "type": "bool"
          },
          {
            "name": "orderTime",
            "type": "u64"
          }
        ]
      }
    },
    {
      "name": "ErrorCode",
      "type": {
        "kind": "enum",
        "variants": [
          {
            "name": "OptionSettled"
          },
          {
            "name": "OptionNotSettled"
          },
          {
            "name": "OptionSettledOrMessed"
          },
          {
            "name": "OptionMintMismatch"
          },
          {
            "name": "ExpiryInThePast"
          },
          {
            "name": "ExpiryInTheFuture"
          },
          {
            "name": "InvalidAsset"
          },
          {
            "name": "InvalidOracle"
          },
          {
            "name": "PriceError"
          },
          {
            "name": "CloseTradeError"
          },
          {
            "name": "CloseMatchingError"
          },
          {
            "name": "CancelOrderDetailsError"
          },
          {
            "name": "LongQuantityError"
          },
          {
            "name": "ShortQuantityError"
          },
          {
            "name": "LongOrderBookError"
          },
          {
            "name": "ShortOrderBookError"
          },
          {
            "name": "LongOrderBookFullError"
          },
          {
            "name": "ShortOrderBookFullError"
          },
          {
            "name": "InsufficientFundError"
          }
        ]
      }
    }
  ]
};

export const IDL: Escrow = {
  "version": "0.1.0",
  "name": "escrow",
  "instructions": [
    {
      "name": "initializeOption",
      "accounts": [
        {
          "name": "initializer",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "openNewTrade",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "traderMintPda",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "optionAccount",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "quantity",
          "type": "u64"
        },
        {
          "name": "price",
          "type": "u64"
        },
        {
          "name": "isLong",
          "type": "bool"
        },
        {
          "name": "isClose",
          "type": "bool"
        },
        {
          "name": "traderMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "cancelTrade",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "optionAccount",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "quantity",
          "type": "u64"
        },
        {
          "name": "price",
          "type": "u64"
        },
        {
          "name": "isLong",
          "type": "bool"
        },
        {
          "name": "isClose",
          "type": "bool"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "matchOrder",
      "accounts": [
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longTrader",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "longTraderMintPda",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "longTraderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longTraderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortTrader",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortTraderMintPda",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortTraderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortTraderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "amount",
          "type": "u64"
        },
        {
          "name": "longTraderMintBump",
          "type": "u8"
        },
        {
          "name": "shortTraderMintBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "settleOption",
      "accounts": [
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "oracleAssetInfo",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "oracleAssetPriceInfo",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        }
      ]
    },
    {
      "name": "collectOption",
      "accounts": [
        {
          "name": "trader",
          "isMut": true,
          "isSigner": true
        },
        {
          "name": "traderMintPda",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderLongMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "traderShortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "optionAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "vaultAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "longMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "shortMintAccount",
          "isMut": true,
          "isSigner": false
        },
        {
          "name": "associatedTokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "tokenProgram",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "rent",
          "isMut": false,
          "isSigner": false
        },
        {
          "name": "systemProgram",
          "isMut": false,
          "isSigner": false
        }
      ],
      "args": [
        {
          "name": "oracleAssetKey",
          "type": "publicKey"
        },
        {
          "name": "expiry",
          "type": "u64"
        },
        {
          "name": "strike",
          "type": "u64"
        },
        {
          "name": "strikeExponent",
          "type": "i32"
        },
        {
          "name": "amount",
          "type": "u64"
        },
        {
          "name": "longMintBump",
          "type": "u8"
        },
        {
          "name": "shortMintBump",
          "type": "u8"
        },
        {
          "name": "oaBump",
          "type": "u8"
        },
        {
          "name": "vaultBump",
          "type": "u8"
        },
        {
          "name": "traderMintBump",
          "type": "u8"
        }
      ]
    }
  ],
  "accounts": [
    {
      "name": "optionAccount",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "optionLister",
            "type": "publicKey"
          },
          {
            "name": "oracleAssetKey",
            "type": "publicKey"
          },
          {
            "name": "vaultAccount",
            "type": "publicKey"
          },
          {
            "name": "longMint",
            "type": "publicKey"
          },
          {
            "name": "shortMint",
            "type": "publicKey"
          },
          {
            "name": "expiry",
            "type": "u64"
          },
          {
            "name": "strike",
            "type": "u64"
          },
          {
            "name": "strikeExponent",
            "type": "i32"
          },
          {
            "name": "oaBump",
            "type": "u8"
          },
          {
            "name": "vaultBump",
            "type": "u8"
          },
          {
            "name": "longMintBump",
            "type": "u8"
          },
          {
            "name": "shortMintBump",
            "type": "u8"
          },
          {
            "name": "longOrderbook",
            "type": {
              "vec": {
                "defined": "OrderInfo"
              }
            }
          },
          {
            "name": "shortOrderbook",
            "type": {
              "vec": {
                "defined": "OrderInfo"
              }
            }
          },
          {
            "name": "isSettled",
            "type": "bool"
          },
          {
            "name": "winningMint",
            "type": "publicKey"
          },
          {
            "name": "isTie",
            "type": "bool"
          }
        ]
      }
    }
  ],
  "types": [
    {
      "name": "OrderInfo",
      "type": {
        "kind": "struct",
        "fields": [
          {
            "name": "trader",
            "type": "publicKey"
          },
          {
            "name": "traderMintBump",
            "type": "u8"
          },
          {
            "name": "traderLongMintAccount",
            "type": "publicKey"
          },
          {
            "name": "traderShortMintAccount",
            "type": "publicKey"
          },
          {
            "name": "quantity",
            "type": "u64"
          },
          {
            "name": "price",
            "type": "u64"
          },
          {
            "name": "isLong",
            "type": "bool"
          },
          {
            "name": "isClose",
            "type": "bool"
          },
          {
            "name": "orderTime",
            "type": "u64"
          }
        ]
      }
    },
    {
      "name": "ErrorCode",
      "type": {
        "kind": "enum",
        "variants": [
          {
            "name": "OptionSettled"
          },
          {
            "name": "OptionNotSettled"
          },
          {
            "name": "OptionSettledOrMessed"
          },
          {
            "name": "OptionMintMismatch"
          },
          {
            "name": "ExpiryInThePast"
          },
          {
            "name": "ExpiryInTheFuture"
          },
          {
            "name": "InvalidAsset"
          },
          {
            "name": "InvalidOracle"
          },
          {
            "name": "PriceError"
          },
          {
            "name": "CloseTradeError"
          },
          {
            "name": "CloseMatchingError"
          },
          {
            "name": "CancelOrderDetailsError"
          },
          {
            "name": "LongQuantityError"
          },
          {
            "name": "ShortQuantityError"
          },
          {
            "name": "LongOrderBookError"
          },
          {
            "name": "ShortOrderBookError"
          },
          {
            "name": "LongOrderBookFullError"
          },
          {
            "name": "ShortOrderBookFullError"
          },
          {
            "name": "InsufficientFundError"
          }
        ]
      }
    }
  ]
};

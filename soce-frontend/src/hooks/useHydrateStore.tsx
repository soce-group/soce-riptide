import { useEffect } from "react";
import useSoceStore, { SoceStore } from "../stores/useSoceStore";
import useInterval from "./useInterval";
import { actionsSelector } from "../stores/selectors";

const useHydrateStore = () => {
  const setSoceStore = useSoceStore((s: SoceStore) => s.set);
  const actions = useSoceStore(actionsSelector);

  useEffect(() => {
    actions.fetchAllSoceAccounts();
  }, [actions]);

  useInterval(() => {
    console.log("Fetch all the other things");
  }, 12 * 30);

  // fetch filled trades for selected market
};

export default useHydrateStore;

import { useEffect, useMemo } from "react";
import Wallet from "@project-serum/sol-wallet-adapter";
import useLocalStorageState from "./useLocalStorageState";
import useSoceStore from "../stores/useSoceStore";
import { notify } from "../utils/notifications";
import { WalletAdapter } from "../@types/types";
import { useTranslation } from "react-i18next";
import { DEFAULT_PROVIDER, WALLET_PROVIDERS } from "../utils/wallet-adapters";

export const PROVIDER_LOCAL_STORAGE_KEY = "walletProvider-0.1";

export default function useWallet() {
  const { t } = useTranslation("common");
  const setSoceStore = useSoceStore((state) => state.set);
  // const {
  //   current: wallet,
  //   connected,
  //   providerUrl: selectedProviderUrl,
  // } = useSoceStore((state) => state.wallet);
  // const endpoint = useSoceStore((state) => state.connection.endpoint);
  // const actions = useSoceStore((s) => s.actions);

  // const [savedProviderUrl, setSavedProviderUrl] = useLocalStorageState(
  //   PROVIDER_LOCAL_STORAGE_KEY,
  //   DEFAULT_PROVIDER.url
  // );
  // const provider = useMemo(
  //   () => WALLET_PROVIDERS.find(({ url }) => url === savedProviderUrl),
  //   [savedProviderUrl]
  // );

  // useEffect(() => {
  //   if (selectedProviderUrl) {
  //     setSavedProviderUrl(selectedProviderUrl);
  //   }
  // }, [selectedProviderUrl]);

  // useEffect(() => {
  //   if (provider) {
  //     const updateWallet = () => {
  //       // hack to also update wallet synchronously in case it disconnects
  //       // eslint-disable-next-line react-hooks/exhaustive-deps
  //     //   const wallet = new (provider.adapter || Wallet)(
  //     //     savedProviderUrl,
  //     //     endpoint
  //     //   ) as WalletAdapter;
  //     //   setSoceStore((state) => {
  //     //     state.wallet.current = wallet;
  //     //   });
  //     };

  //     if (document.readyState !== "complete") {
  //       // wait to ensure that browser extensions are loaded
  //       const listener = () => {
  //         updateWallet();
  //         window.removeEventListener("load", listener);
  //       };
  //       window.addEventListener("load", listener);
  //       return () => window.removeEventListener("load", listener);
  //     } else {
  //       updateWallet();
  //     }
  //   }
  // }, [provider, savedProviderUrl, endpoint]);

  // useEffect(() => {
  //   if (!wallet) return;
  //   wallet.on("connect", async () => {
  //     setSoceStore((state) => {
  //       state.wallet.connected = true;
  //     });
  //     // TODO: Add functionality to hydrate the store
  //   });
  //   wallet.on("disconnect", () => {
  //     console.log("disconnecting wallet");
  //     setSoceStore((state) => {
  //       state.wallet.connected = false;
  //     });
  //     notify({
  //       type: "info",
  //       title: t("wallet-disconnected"),
  //     });
  //   });
  // }, [wallet, setSoceStore]);

  // return { connected, wallet };
  return { todo: "Remove" };
}
